<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
if(isset($_GET["msg"])) {
    $msg  =	$_GET["msg"];
}
if($msg==1) {
	$message	=	"User has been added successfully.";
} elseif($msg==2) {
	$message	=	"User has been updated successfully.";
} elseif($msg==3) {
	$message	=	"User has been deleted successfully.";
}
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
		if(!empty($message)) {
		?>
			<div class="alert alert-success">
					<a class="close" data-dismiss="alert" href="#">x</a>
		<?php echo $message;?>
			</div>
		<?php
		}
	?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/employee.png" class="imgbasline"> Users List</div>
            <div class="actions">
            	<a href="export_user_list.xls" class="btn green btn-sm excelbtn"><i class="fa fa-download"></i> Export to Excel</a>
                <a href="add_user.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add User</a>
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="email" id="email" placeholder="Email Address">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="mobile" id="mobile" placeholder="Mobile">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
	                        <select class="form-control" name="role" id="role">
                                <option value="">Select Role</option>
                                <option value="1">Admin</option>
                                <option value="2">User</option>
                            </select>
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
	                        <select id="sel_status" name="sel_status" class="form-control">
	                        	<option value="">Select Status</option>
	                        	<option value="Enable">Enable</option>
	                        	<option value="Disable">Disable</option>
	                        </select>
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<a href="users_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover" id="tblemployee">
	            	<thead>
	                    <tr>
	                        <th> SI.NO </th>
	                        <th> First Name </th>
	                        <th> Last Name </th>
	                        <th> Email </th>
	                        <th> Mobile </th>
	                        <th> Role</th>
	                        <th> Status </th>
	                        <th> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                        <td> 1 </td>
	                        <td> Pushpa </td>
	                        <td> Prahathese </td>
	                        <td> prahathese.shenll@gmail.com</td>
	                        <td> 9876545630</td>
	                        <td> Admin</td>
	                        <td><span class="label label-sm label-success labelboader"> Enable </span> </td>
	                        <td> <a href="edit_user.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 2 </td>
	                        <td> Rajesh </td>
	                        <td> Kannan </td>
	                        <td> rajesh@gmail.com</td>
	                        <td> 9876545631</td>
	                        <td> User</td>
	                        <td><span class="label label-sm label-danger labelboader"> Disable </span> </td>
	                        <td> <a href="edit_user.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
	$('#tblemployee').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();
    $( function() {
      $("#from_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $( function() {
      $("#to_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
     $( function() {
      $("#expire_from").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $( function() {
      $("#expire_to").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
</script>