<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/disposal.png" class="imgbasline"> Items Disposal/Destruction</div>
            <div class="actions">
                <!-- <a href="add_sub_subcategory.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Sub Sub-Category</a> -->
            </div>
        </div>
        <div class="portlet-body">
	        <form name="frm_data" id="frm_data" action="store_return.php" class="form-horizontal" method="POST">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Store Name <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select  class="form-control" name="supplier_code" id="supplier_code">
                                <option value="">Select Store</option>
                                <option value="1">Al Meera</option>
                                <option value="2">Lulu Salmiya</option>
                                <option value="3">Defence Co Op</option>
                                <option value="4">Carrefour</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Transfers Id <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select  class="form-control" name="transfers_code" id="transfers_code">
                                <option value="">Select Transfers Id</option>
                                <option value="TRID7001">TRID7001</option>
                                <option value="TRID7002">TRID7002</option>
                                <option value="TRID7003">TRID7003</option>
                                <option value="TRID7004">TRID7004</option>
                            </select>
                        </div>
                    </div>
                    <div class="storereturndisp" style="display:none">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table-hover" id="tblreturnstore">
                                    <thead>
                                        <tr>
                                            <th>Category</th>
                                            <th>Sub Category</th>
                                            <th>Sub Sub-Category</th>
                                            <th>Serial No</th>
                                            <th>Item</th>
                                            <th>Quantity</th>
                                            <!-- <th>Description</th> -->
                                            <th>Unit Price</th>
                                            <th>Total</th>
                                            <th>Disposal/Destruction Qty</th>
                                            <th>
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox" class="checkboxes" name="export_all" onclick="checkAllExp(this)">
                                                    <span></span>
                                                </label>
                                           </th>
                                            <!--<th>Action</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Electronics</td>
                                            <td>Apple Phone</td>
                                            <td>Mobile Panel</td>
                                            <td>18763567</td>
                                            <td>Apple</td>
                                            <td>10</td>
                                            <!-- <td>good</td> -->
                                            <td>1000</td>
                                            <td>10,0000</td>
                                            <td><input type="text" class="form-control" name="return_qty" id="return_qty" placeholder="Return Quantity" value="5"></td>
                                            <td>
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox" name="checkbox[]" id="checkbox" value="1" class="checkboxes" onclick="eachChk()">
                                                    <span></span>
                                                </label>  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> Home Appliance </td>
                                            <td> Chair </td>
                                            <td> Wheel Chair</td>
                                            <td> 18763568</td>
                                            <td> wood </td>
                                            <td> 1</td>
                                            <!-- <td>good </td> -->
                                            <td> 100 </td>
                                            <td> 100</td>
                                            <td><input type="text" class="form-control" name="return_qty" id="return_qty" placeholder="Return Quantity" value="0"></td>
                                            <td>
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox" name="checkbox[]" id="checkbox" value="1" class="checkboxes" onclick="eachChk()">
                                                    <span></span>
                                                </label>  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> Food Products </td>
                                            <td> Ice Cream </td>
                                            <td> Cup Ice</td>
                                            <td> 18763569</td>
                                            <td> Blackberry </td>
                                            <td> 4</td>
                                            <!-- <td> good </td> -->
                                            <td> 50 </td>
                                            <td> 200</td>
                                            <td><input type="text" class="form-control" name="return_qty" id="return_qty" placeholder="Return Quantity" value="2"></td>
                                            <td>
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox" name="checkbox[]" id="checkbox" value="1" class="checkboxes" onclick="eachChk()">
                                                    <span></span>
                                                </label>  
                                            </td>
                                           <!--  <td><textarea class="form-control" name="remark" id="remark" placeholder="Remark">Damaged</textarea></td>
                                            <td><button type="submit" class="btn green customsavebtn"><i class="fa fa-check"></i> Return </button></td> -->
                                        </tr>
                                        <tr>
                                            <td> Stationary </td>
                                            <td> Pencil </td>
                                            <td> HP Pencil</td>
                                            <td> 18763569</td>
                                            <td> HP </td>
                                            <td> 5 </td>
                                            <!-- <td> good </td> -->
                                            <td> 5 </td>
                                            <td> 25 </td>
                                           <td><input type="text" class="form-control" name="return_qty" id="return_qty" placeholder="Return Quantity" value="3"></td>
                                           <td>
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox" name="checkbox[]" id="checkbox" value="1" class="checkboxes" onclick="eachChk()">
                                                    <span></span>
                                                </label>  
                                            </td>
                                            <!-- <td><textarea class="form-control" name="remark" id="remark" placeholder="Remark">Damaged</textarea></td>
                                            <td><button type="submit" class="btn green customsavebtn"><i class="fa fa-check"></i> Return </button></td> -->
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <button type="submit" class="btn green customsavebtn adminapprovel">
                                                <i class="fa fa-check"></i> Save
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>
<script>
    $(document).on("change","#transfers_code",function(){
       var id= $(this).val();
        if(id !=""){
            $(".storereturndisp").css("display","block");
        } else {
            $(".storereturndisp").css("display","none");
        }
    });
    function checkAllExp(Element)
    {
        chk_len=$('input[id=checkbox]').length;
        if(Element.checked){
            if(chk_len>0){
                $('input[id=checkbox]').prop('checked', true);
            }
        }
        else{
            if(chk_len>0){
                $('input[id=checkbox]').prop('checked', false);
            }
        }
    }
    function eachChk()
    {
        chk_len=$('input[id=checkbox]').length;
        chk_len1=$('input[id=checkbox]:checked').length;
        if(chk_len==chk_len1){
            $('input[name=export_all]').prop('checked', true);
        }
        else{
            $('input[name=export_all]').prop('checked', false);
        }
    }
    // var var;
$(document).ready(function() {
    $(".adminapprovel").click(function(){
        chk_len = $('input[id=checkbox]:checked').length;
        if(chk_len==0)
        {
            alert("Please select atleast one Items for Disposal/Destruction ?");
            return false;
        } else {
            return false;
        }
    });
});

</script>