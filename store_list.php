<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
if(isset($_GET["msg"])) {
    $msg  =	$_GET["msg"];
}
if($msg==1) {
	$message	=	"Store has been added successfully.";
} elseif($msg==2) {
	$message	=	"Store has been updated successfully.";
} elseif($msg==3) {
	$message	=	"Store has been deleted successfully.";
}
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
		if(!empty($message)) {
		?>
			<div class="alert alert-success">
					<a class="close" data-dismiss="alert" href="#">x</a>
		<?php echo $message;?>
			</div>
		<?php
		}
	?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/warehouse.png" class="imgbasline"> Store List </div>
            <div class="actions">
            	<a href="export_store_list.xls" class="btn green btn-sm excelbtn"><i class="fa fa-download"></i> Export to Excel</a>
                <a href="add_store.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Store </a>
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="store_name" id="store_name" placeholder="Store Name">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<select class="form-control select2" name="store_type" id="store_type" >
                        		<option value="Supplier">Apple Program</option>
                        		<option value="Supplier">Deployment </option>
                        		<option value="Supplier">Maintenance  </option>
                        		<option value="Supplier">BA merchandising </option>
                        		<option value="Supplier">Special projects</option>
                        		<option value="Supplier">NPI</option>
                        	</select>
                        </div>
	        		</div>
	        		 <div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<select class="form-control select2" name="store_name" id="store_name" >
                        		<option value="Supplier">Select Country</option>
                        		<option value="Supplier">UAE </option>
                        		<option value="Supplier">KSA_Riyadh </option>
                        		<option value="Supplier">KSA_Jeddah </option>
                        		<option value="Supplier">KSA_Dammam</option>
                        		<option value="Supplier">Oman</option>
                        		<option value="Supplier">Qatar</option>
                        		<option value="Supplier">Kuwait</option>
                        		<option value="Supplier">Bahrain</option>
                        	</select>
                        </div>
	        		</div>
	        		<!-- <div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="region" id="region" placeholder="Region">
                        </div>
	        		</div> -->
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="city" id="city" placeholder="City">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
	                        <select id="sel_status" name="sel_status" class="form-control">
	                        	<option value="">Select Status</option>
	                        	<option value="Enable">Enable</option>
	                        	<option value="Disable">Disable</option>
	                        </select>
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<a href="store_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover" id="tblrole">
	            	<thead>
	                    <tr>
	                        <th> SI.NO </th>
	                        <th> Store Name </th>
	                        <th> Apple Program </th>
	                        <th> Country </th>
	                        <th> City </th>
	                        <th> Account Name </th>
	                       <!--  <th> Channel </th> -->
	                        <th> Status </th>
	                        <th> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                        <td> 1 </td>
	                        <td> Al Meera </td> 
	                        <td> Deployment </td>
	                        <td> UAE </td>
	                        <td> Dubhai </td>
	                        <td> UAE Acc </td> 
	                       <!--  <td> SY </td>  -->
	                        <td><span class="label label-sm label-success labelboader"> Enable </span> </td>
	                        <td> <a href="edit_store.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 2 </td>
	                        <td>  Lulu Salmiya  </td>
	                        <td> Maintenance </td>
	                        <td> KSA_Riyadh </td>
	                        <td> Abudhaabi </td>
	                        <td> Riyaadh Acc </td>
	                        <!-- <td> AP </td>	     -->                    
	                        <td><span class="label label-sm label-danger labelboader"> Disable </span> </td>
	                        <td> <a href="edit_store.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 3 </td>
	                         <td> Fine Line </td>
	                        <td> merchandising </td>
	                        <td> KSA_Jeddah </td>
	                        <td> Sharah </td>
	                        <td> Jeddah Acc </td>
	                        <!-- <td> BL </td> -->
	                        <td><span class="label label-sm label-success labelboader"> Enable </span> </td>
	                        <td> <a href="edit_store.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                     <tr>
	                        <td> 4 </td>
	                        <td> Classic </td>
	                        <td> Special projects </td>
	                        <td> KSA_Dammam </td>
	                        <td> Ajman </td>
	                        <td> Dammam Acc </td>
	                        <!-- <td> NK </td> -->
	                        <td><span class="label label-sm label-danger labelboader"> Disable </span> </td>
	                        <td> <a href="edit_store.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                     <tr>
	                        <td> 5 </td>
	                        <td> Defence Co Op  </td>
	                        <td> NPI </td>
	                        <td> Oman </td>
	                        <td> Masdar city </td>
	                        <td> Oman Acc </td>
	                        <!-- <td> NK </td> -->
	                        <td><span class="label label-sm label-danger labelboader"> Disable </span> </td>
	                        <td> <a href="edit_store.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
	$('#tblrole').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();

    $(document).ready(function() {
    $('.select2-hidden-accessible').select2();
    });
</script>