<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
if(isset($_GET["msg"])) {
    $msg  =	$_GET["msg"];
}
if($msg==1) {
	$message	=	"Category has been added successfully.";
} elseif($msg==2) {
	$message	=	"Category has been updated successfully.";
} elseif($msg==3) {
	$message	=	"Category has been deleted successfully.";
}
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
		if(!empty($message)) {
		?>
			<div class="alert alert-success">
					<a class="close" data-dismiss="alert" href="#">x</a>
		<?php echo $message;?>
			</div>
		<?php
		}
	?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/area.png" class="imgbasline"> Category List</div>
            <div class="actions">
            	<a href="export_category_list.xls" class="btn green btn-sm excelbtn"><i class="fa fa-download"></i> Export to Excel</a>
                <a href="add_category.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Category</a>
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="category_name" id="category_name" placeholder="Category Name">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
	                        <select id="sel_status" name="sel_status" class="form-control">
	                        	<option value="">Select Status</option>
	                        	<option value="Enable">Enable</option>
	                        	<option value="Disable">Disable</option>
	                        </select>
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<a href="category_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover" id="tblrole">
	            	<thead>
	                    <tr>
	                        <th style="width:35px;"> SI.NO </th>
	                        <th> Category Name </th>
	                        <th style="width:100px;"> Status </th>
	                        <th style="width:200px;"> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                        <td> 1 </td>
	                        <td> Iphone </td>
	                        <td><span class="label label-sm label-success labelboader"> Enable </span> </td>
	                        <td> <a href="edit_category.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 2 </td>
	                        <td> Apple TV </td>
	                        <td><span class="label label-sm label-danger labelboader"> Disable </span> </td>
	                        <td> <a href="edit_category.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 3 </td>
	                        <td> IPAD </td>
	                        <td><span class="label label-sm label-success labelboader"> Enable </span> </td>
	                        <td> <a href="edit_category.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                     <tr>
	                        <td> 4 </td>
	                        <td> Mac Book </td>
	                        <td><span class="label label-sm label-success labelboader"> Enable </span> </td>
	                        <td> <a href="edit_category.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
	$('#tblrole').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();
</script>