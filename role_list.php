<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
if(isset($_GET["msg"])) {
    $msg  =	$_GET["msg"];
}
if($msg==1) {
	$message	=	"Role has been added successfully.";
} elseif($msg==2) {
	$message	=	"Role has been updated successfully.";
} elseif($msg==3) {
	$message	=	"Role has been deleted successfully.";
}
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
		if(!empty($message)) {
		?>
			<div class="alert alert-success">
					<a class="close" data-dismiss="alert" href="#">x</a>
		<?php echo $message;?>
			</div>
		<?php
		}
	?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/setting.png" class="imgbasline"> Role List</div>
            <div class="actions">
                <a href="add_role.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Role</a>
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="role_name" id="role_name" placeholder="Role Name">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="role_description" id="role_description" placeholder="Role Description">
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<a href="role_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover" id="tblrole">
	            	<thead>
	                    <tr>
	                        <th style="width:35px;"> SI.NO </th>
	                        <th> Role Name </th>
	                        <th> Role Description</th>
	                        <th style="width:150px;"> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                        <td> 1 </td>
	                        <td> Admin </td>
	                        <td> Accessing Admin Panel </td>
	                        <td> <a href="edit_role.php?id=1" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 2 </td>
	                        <td> User </td>
	                        <td> Accessing User Panel  </td>
	                        <td> <a href="edit_role.php?id=2" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                   
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
	$('#tblrole').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();
</script>