<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/vendor.png" class="imgbasline"> Edit Supplier</div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_supplier" id="frm_supplier" action="supplier_list.php?msg=2" class="form-horizontal" method="POST">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Supplier Code<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="supplier_code" id="supplier_code" placeholder="Supplier Code" value="10609">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Supplier Name <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="Supplier_name" id="supplier_name" placeholder="Supplier Name" value="Arvato">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Email <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="abb@gmail.com">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3">Phone<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" value="9876545630">
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3">Country<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control select2" name="country" id="country" >
                                <option value="Supplier">Select Country</option>
                                <option value="Supplier" selected>UAE </option>
                                <option value="Supplier">KSA_Riyadh </option>
                                <option value="Supplier">KSA_Jeddah </option>
                                <option value="Supplier">KSA_Dammam</option>
                                <option value="Supplier">Oman</option>
                                <option value="Supplier">Qatar</option>
                                <option value="Supplier">Kuwait</option>
                                <option value="Supplier">Bahrain</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">City<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="city" id="city" placeholder="City" value="Abu Dhabi">
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="control-label col-md-3">State<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="state" id="state" placeholder="State" value="Dubai">
                        </div>
                    </div> -->
                   <!--  <div class="form-group">
                        <label class="control-label col-md-3">Region<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="region" id="region" placeholder="Region" value="Abu Dhabi">
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="control-label col-md-3">Account Name<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="account_name" id="account_name" placeholder="Account Name" value="Direct Retail">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Address <span class="required" aria-required="true">  </span>
                        </label>
                        <div class="col-md-4">
                            <textarea type="text" class="form-control" name="address" id="address" placeholder="Address" cols="5" rows="5">Sheikh Zayed Road, 6th Interchange, Exit # 25, Jebel Ali, DubaiLandmark: Near Oasis Pure Water Co Zip Code: 3627</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Status<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="optionsRadios" id="optionsRadios25" value="option1" checked>Enable
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="optionsRadios" id="optionsRadios26" value="option2"> Disable
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn green customsavebtn">
                                <i class="fa fa-check"></i> Save
                            </button>
                            <a href="supplier_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>