<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/reports.png" class="imgbasline"> Total Stock Returns</div>
            <div class="actions">
               <a href="javascript:void(0);" class="btn green btn-sm customviewbtn bulkpopup"><i class="fa fa-print"></i> Print </a>
                <a href="total_return.xls" class="btn green btn-sm excelbtn"><i class="fa fa-download"></i> Export to Excel</a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12 paddingleftright">
                    <!-- <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <select class="form-control select2" name="store_name" id="store_name" >
                                <option value="Supplier">Select Country</option>
                                <option value="Supplier">UAE </option>
                                <option value="Supplier">KSA_Riyadh </option>
                                <option value="Supplier">KSA_Jeddah </option>
                                <option value="Supplier">KSA_Dammam</option>
                                <option value="Supplier">Oman</option>
                                <option value="Supplier">Qatar</option>
                                <option value="Supplier">Kuwait</option>
                                <option value="Supplier">Bahrain</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <select class="form-control select2" name="store_type" id="store_type" >
                                <option value="Supplier">Select Apple Program</option>
                                <option value="Supplier">Deployment </option>
                                <option value="Supplier">Maintenance  </option>
                                <option value="Supplier">BA merchandising </option>
                                <option value="Supplier">Special projects</option>
                                <option value="Supplier">NPI</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="part_number" id="part_number" placeholder="Part Number">
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="part_des" id="part_des" placeholder="Part Description">
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="apple_id" id="apple_id" placeholder="Apple ID">
                        </div>
                    </div> -->
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <select class="form-control select2" name="outlet_name" id="outlet_name">
                                <option value="">Select Outlet</option>
                                <option value="1">Axiom Delma Mall </option>
                                <option value="2">Al Thalla Cafeteria</option>
                                <option value="3">Ashjan Grocery</option>
                                <option value="4">Shafi Grocery</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <select class="form-control select2" name="supplier_name" id="supplier_name">
                                <option value="">Select Store</option>
                                 <option value="1">Al Meera</option>
                                <option value="2">Lulu Salmiya</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="part_number" id="part_number" placeholder="Part Number">
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="part_des" id="part_des" placeholder="Part Description">
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="apple_id" id="apple_id" placeholder="Apple ID">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="col-md-12 paddingleftright">
                            <button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
                            <a href="total_returns.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
                <table class="table table-striped table-bordered table-hover" id="tblrole">
                    <thead>
                        <tr>
                            <th nowrap> SI.NO </th>
                            <th nowrap> Outlet</th>
                            <th nowrap> Store</th>
                            <th nowrap> Part Number </th>
                            <th nowrap> Part Description </th>                        
                            <th nowrap> Qty </th>
                            <th nowrap> Remarks </th>
                            <th nowrap> Return Date </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td> 1 </td>
                            <td nowrap>Axiom Delma Mall</td>
                            <td nowrap> Al Meera </td>
                            <td nowrap> 622-00084 </td>
                            <td nowrap> Apple TV Base Unit </td>
                            <td> 7 </td>
                            <td nowrap> QATAR APR SPARES </td>
                            <td> 23/01/2019</td>
                        </tr>
                        <tr>
                            <td> 2 </td>
                            <td nowrap> Al Thalla Cafeteria </td>
                            <td nowrap> Al Meera </td>
                            <td> 677-01423 </td>
                            <td nowrap> Apple TV Cable Clip </td>
                            <td> 8 </td>
                            <td nowrap> IQ VILLAGIO MALL </td>
                            <td> 20/01/2019</td>
                        </tr>
                        <tr>
                            <td> 3 </td>
                            <td nowrap> Ashjan Grocery </td>
                            <td> Al Meera </td>
                            <td> 622-00084 </td>
                            <td nowrap> USBC-USBA Cables </td>
                            <td> 5 </td>
                            <td nowrap> QATAR APPLE SHOP 2.0 SPARE </td>
                            <td> 19/01/2019</td>
                        </tr>
                        <tr>
                            <td> 4 </td>
                            <td nowrap> Shafi Grocery </td>
                            <td> Al Meera </td>
                            <td nowrap> GC24806A-MEAR</td>
                            <td nowrap> SR IPAD PRO WARM GPF24</td>
                            <td> 5 </td>
                            <td> QATAR MOBILITY SPARES</td>
                            <td> 19/01/2019</td>
                        </tr>
                         <tr>
                            <td> 5 </td>
                             <td nowrap>Axiom Delma Mall</td>
                            <td nowrap> Lulu Salmiya </td>
                            <td> 622-00084 </td>
                            <td> Sekure Flat Tab Sensor </td>
                            <td> 7 </td>
                            <td> QATAR APR SPARES </td>
                            <td> 23/01/2019</td>
                        </tr>
                        <tr>
                            <td> 6 </td>
                             <td nowrap> Al Thalla Cafeteria </td>
                            <td nowrap> Lulu Salmiya </td>
                            <td> 677-01423 </td>
                            <td>MTI 2.0/MTI 3.0 Security MCP Unit</td>
                            <td> 8 </td>
                            <td> IQ VILLAGIO MALL </td>
                            <td> 20/01/2019</td>
                        </tr>
                        <tr>
                            <td> 7 </td>
                             <td> Ashjan Grocery </td>
                            <td> Lulu Salmiya </td>
                            <td> 622-00084 </td>
                            <td> USBC-USBA Cables </td>
                            <td> 5 </td>
                            <td> QATAR APPLE SHOP 2.0 SPARE </td>
                            <td> 19/01/2019</td>
                        </tr>
                        <tr>
                            <td> 8 </td>
                            <td> Shafi Grocery </td>
                            <td> Lulu Salmiya </td>
                            <td> GC24806A-MEAR</td>
                            <td> MTI 3.0 Intellikey(USB)</td>
                            <td> 5 </td>
                            <td> QATAR MOBILITY SPARES</td>
                            <td> 19/01/2019</td>
                        </tr>
                         <tr>
                            <td> 9 </td>
                            <td> Axiom Delma Mall </td>
                            <td> Al Meera </td>
                            <td> 622-00084 </td>
                            <td> iPad Pro 9.7" MTI 3.0 bracket </td>
                            <td> 7 </td>
                            <td> QATAR APR SPARES </td>
                            <td> 23/01/2019</td>
                        </tr>
                        <tr>
                            <td> 10 </td>
                            <td>Al Thalla Cafeteria</td>
                            <td>Al Meera</td>
                            <td> 677-01423 </td>
                            <td> IOS Dock Adhesive removal tool</td>
                            <td> 8 </td>
                            <td> IQ VILLAGIO MALL </td>
                            <td> 20/01/2019</td>
                        </tr>
                        <tr>
                            <td> 11 </td>
                            <td>Ashjan Grocery</td>
                            <td> Al Meera </td>
                            <td> 622-00084 </td>
                            <td> A4 Acrylic </td>
                            <td> 5 </td>
                            <td> QATAR APPLE SHOP 2.0 SPARE </td>
                            <td> 19/01/2019</td>
                        </tr>
                        <tr>
                            <td> 12 </td>
                            <td>Shafi Grocery</td>
                            <td>Al Meera</td>
                            <td> GC24806A-MEAR</td>
                            <td>MB USB-C Combo Cable 1/EA</td>
                            <td> 5 </td>
                            <td> QATAR MOBILITY SPARES</td>
                            <td> 19/01/2019</td>
                        </tr>

                         <tr>
                            <td> 13 </td>
                             <td> Axiom Delma Mall </td>
                            <td> Lulu Salmiya </td>
                            <td> 622-00084 </td>
                            <td>Security Cable : Smart Keyboard</td>
                            <td> 7 </td>
                            <td> QATAR APR SPARES </td>
                            <td> 23/01/2019</td>
                        </tr>
                        <tr>
                            <td> 14 </td>
                             <td>Al Thalla Cafeteria</td>
                            <td> Lulu Salmiya </td>
                            <td> 677-01423 </td>
                            <td> IPD combo cable 1/EA UTE</td>
                            <td> 8 </td>
                            <td> IQ VILLAGIO MALL </td>
                            <td> 20/01/2019</td>
                        </tr>
                        <tr>
                            <td> 15 </td>
                            <td>Ashjan Grocery</td>
                            <td> Lulu Salmiya </td>
                            <td> 622-00084 </td>
                            <td>iPad Pro 12.9" Puck Locator</td>
                            <td> 5 </td>
                            <td> QATAR APPLE SHOP 2.0 SPARE </td>
                            <td> 19/01/2019</td>
                        </tr>
                        <tr>
                            <td> 16 </td>
                            <td>Shafi Grocery</td>
                            <td> Lulu Salmiya </td>
                            <td> GC24806A-MEAR</td>
                            <td> PRO Security Cable 1EA</td>
                            <td> 5 </td>
                            <td> QATAR MOBILITY SPARES</td>
                            <td> 19/01/2019</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
  $(document).ready(function() {
    $('#tblrole').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":20,
        "ordering": false
    } );    
    } );

    $("#search_result_length").hide();
    $(document).ready(function() {
        $('.select2-hidden-accessible').select2();
    });
    $( function() {
      $("#from_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $( function() {
      $("#to_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Store Wise Total Returns Quantity.'
    },
    subtitle: {
        // text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
    },
    xAxis: {
        type: 'category',
        title: {
            text: 'Part Name'
        }
    },
    yAxis: {
        title: {
            text: 'Store wise Returns'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:f}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> of Quantity<br/>'
    },

    "series": [
        {
            "name": "Part",
            "colorByPoint": true,
            "data": [
                {
                    "name": "Apple TV Base Unit",
                    "y": 8,
                    "drilldown": "Apple TV Base Unit"
                },
                {
                    "name": "Apple TV Cable Clip",
                    "y": 4,
                    "drilldown": "Apple TV Cable Clip"
                },
                {
                    "name": "USBC-USBA Cables",
                    "y": 3,
                    "drilldown": "USBC-USBA Cables"
                },
                {
                    "name": "SR IPAD PRO WARM GPF24",
                    "y": 2,
                    "drilldown": "SR IPAD PRO WARM GPF24"
                },
                {
                    "name": "Sekure Flat Tab Sensor",
                    "y": 7,
                    "drilldown": "Sekure Flat Tab Sensor"
                },
                {
                    "name": "MTI 2.0/MTI 3.0 Security MCP Unit",
                    "y": 2,
                    "drilldown": "MTI 2.0/MTI 3.0 Security MCP Unit"
                },
                {
                    "name": "MTI 3.0 Intellikey(USB)",
                    "y": 9,
                    "drilldown": "MTI 3.0 Intellikey(USB)"
                },
                {
                    "name": "iPad Pro 9.7 MTI 3.0 bracket",
                    "y": 17,
                    "drilldown": "iPad Pro 9.7 MTI 3.0 bracket"
                },
                {
                    "name": "PRO Security Cable 1EA",
                    "y": 13,
                    "drilldown": "IPRO Security Cable 1EA"
                },
                 {
                    "name": "A4 Acrylic",
                    "y": 4,
                    "drilldown": "A4 Acrylic"
                },
                {
                    "name": "MB USB-C Combo Cable 1/EA",
                    "y": 3,
                    "drilldown": "MB USB-C Combo Cable 1/EA"
                },
                {
                    "name": "Smart Keyboard",
                    "y":2,
                    "drilldown": "Smart Keyboard"
                },
                {
                    "name": "IPD combo cable 1/EA UTE",
                    "y": 1,
                    "drilldown": "IPD combo cable 1/EA UTE"
                },
            ]
        }
    ]
});
</script>