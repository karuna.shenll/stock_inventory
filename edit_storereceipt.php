<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/upload.png" class="imgbasline"> Edit Stock Receipt</div>
            <div class="actions">
            </div>
        </div>
        <div class="portlet-body">
           <form name="frm_data" id="frm_data" action="storereceipt_list.php" class="form-horizontal" method="POST">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Store <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select  class="form-control select2" name="store_code" id="store_code">
                                <option value="">Select Store</option>
                                <option value="1" selected="">Al Meera</option>
                                <option value="2">Lulu Salmiya </option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Manufacturer <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select  class="form-control select2" name="supplier_code" id="supplier_code">
                                <option value="">Select Manufacturer</option>
                                <option value="1" selected="">ARAVTO</option>
                                <option value="2">RHEIM</option>
                                <option value="3">Defence Co Op</option>
                                <option value="4">Fusion</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Waybill <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="waybill" id="waybill" placeholder="Waybill" value="1Z7488EE04522244" >
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Received Date <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="requisition_date" id="requisition_date" placeholder="Received Date" autocomplete="off" data-date-format="dd/mm/yyyy" value="29/01/2019">
                        </div>
                    </div>
                    <h3 class="form-section formheading">Items</h3>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="tblstorerecipt">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Part Number</th>
                                            <th>Part Description</th>
                                            <th class="text-center">Apple ID</th>                                            
                                            <th>Quantity</th>
                                            <th>Date</th>
                                            <th>Remarks</th>
                                            <th></th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                             <td>
                                               <input type="text" class="form-control" name="part_number[]" id="part_number" placeholder="Part Number" value="622-00084">
                                                <span style="position: absolute;left: 133px; color: #4285f4; cursor: pointer;padding-right: 10px;" class="addpopup">Add New Part</span>
                                            </td>
                                            <td nowrap>Apple TV Base Unit</td>
                                            <td class="text-center" nowrap> 1093880</td>                                           
                                            <td>
                                                <input type="text" class="form-control" name="quantity[]" id="quantity" placeholder="Quantity" value="4" style="width: 100px;">
                                            </td>
                                            <td> <input type="text" class="form-control stock_date" name="stock_date[]" id="stock_date" placeholder="Date" value="29/01/2019" style="width: 100px;"></td>
                                            <td>
                                               <textarea class="form-control" name="remark[]" id="remark" placeholder="Remarks">QATAR APR SPARES</textarea>
                                           </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                            <td>

                                                <input type="text" class="form-control" name="part_number[]" id="part_number" placeholder="Part Number" value="677-01423">
                                                <span style="position: absolute;left: 133px; color: #4285f4; cursor: pointer;padding-right: 10px;" class="addpopup">Add New Part</span>
                                            </td>
                                            <td nowrap> Apple TV Cable Clip</td>
                                            <td class="text-center" nowrap>1599662</td>
                                            
                                            <td>
                                                <input type="text" class="form-control" name="quantity[]" id="quantity" placeholder="Quantity" value="3" style="width: 100px;">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control stock_date" name="stock_date[]" id="stock_date" placeholder="Date" value="30/01/2019" style="width: 100px;">
                                            </td>
                                            <td>
                                               <textarea class="form-control" name="remark[]" id="remark" placeholder="Remarks">IQ VILLAGIO MALL</textarea>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr>
                                             <td>
                                               <input type="text" class="form-control" name="part_number[]" id="part_number" placeholder="Part Number" value="622-00084">
                                                <span style="position: absolute;left: 133px; color: #4285f4; cursor: pointer;padding-right: 10px;" class="addpopup">Add New Part</span>
                                            </td>
                                            <td nowrap>USBC-USBA Cable</td>
                                            <td class="text-center" nowrap>XXXXQATAPR</td>
                                            
                                            <td><input type="text" class="form-control" name="quantity[]" id="quantity" placeholder="Quantity" value="8" style="width: 100px;"></td>
                                            <td> <input type="text" class="form-control stock_date" name="stock_date[]" id="stock_date" placeholder="Date" value="31/01/2019" style="width: 100px;"></td>
                                            <td>
                                               <textarea class="form-control" name="remark[]" id="remark" placeholder="Remarks">QATAR MOBILITY SPARES</textarea>
                                            </td>
                                            <td></td>
                                        </tr>
                                        <tr class="appendContent">
                                            <td>

                                               <input type="text" class="form-control" name="part_number[]" id="part_number" placeholder="Part Number" value="GC24806A-MEAR">
                                                <span style="position: absolute;left: 133px; color: #4285f4; cursor: pointer;padding-right: 10px;" class="addpopup">Add New Part</span>
                                            </td>
                                            <td nowrap> SR IPAD PRO WARM GPF24</td>
                                            <td class="text-center" nowrap> XXXXQATWHITE</td>
                                            
                                            <td><input type="text" class="form-control" name="quantity[]" id="quantity" placeholder="Quantity" value="6" style="width: 100px;"></td>
                                            <td> <input type="text" class="form-control stock_date" name="stock_date[]" id="stock_date" placeholder="Date" value="01/02/2019" style="width: 100px;"></td>
                                            <td>
                                               <textarea class="form-control" name="remark[]" id="remark" placeholder="Remarks">QATAR APPLE SHOP 2.0 SPARES</textarea>
                                            </td>
                                            <td> <button type="button" class="btn btn-info customaddmorebtn" name="addmore" id="addmore"><span class="glyphicon glyphicon-plus" aria-hidden="true"></span></button></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn green customsavebtn">
                                <i class="fa fa-check"></i> Save
                            </button>
                            <a href="storereceipt_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<div class="modal fade in" id="itempop" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="assing_status" id="assing_status" method="POST">
                <input type="hidden" name="hnd_id" id="hnd_id">
                <input type="hidden" name="hndeid" id="hndeid">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                    <h4 class="modal-title"><b>Add New Part</b></h4>
                </div>
                <div class="modal-body"> 
                    
                    <div class="row">
                        <div class="col-md-11" style="margin-top: 15px;margin-bottom: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4">Part Number</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" name="s_no" id="s_no" placeholder="Part Number" value="622-00084">
                                </div>
                            </div>
                        </div>
                      
                       
                        <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4"> Part Description</label>
                                <div class="col-md-8 vendor-detail">
                                   <textarea id="part_des" name="part_des" placeholder="Part Description" class="form-control">Apple TV Base Unit</textarea>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4"> Apple ID</label>
                                <div class="col-md-8 vendor-detail">
                                   <input type="text" class="form-control" name="apple_id" id="apple_id" placeholder="Apple ID" value="1093880">
                                </div>
                            </div>
                        </div>

                        <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4"> Supplier Name</label>
                                <div class="col-md-8 vendor-detail">
                                    <select class="form-control select2" name="supplier_id" id="supplier_id" >
                                        <option value=""> Select Supplier</option>
                                        <option value="Supplier" selected>Arvato</option>
                                        <option value="Supplier">ARIAN</option>
                                        <option value="Supplier">CCS DIGITAL</option>
                                        <option value="Supplier">FUSION</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4"> Country</label>
                                <div class="col-md-8 vendor-detail">
                                    <select class="form-control select2" name="store_name" id="store_name" >
                                        <option value="Supplier">Select Country</option>
                                        <option value="Supplier" selected>UAE </option>
                                        <option value="Supplier">KSA_Riyadh </option>
                                        <option value="Supplier">KSA_Jeddah </option>
                                        <option value="Supplier">KSA_Dammam</option>
                                        <option value="Supplier">Oman</option>
                                        <option value="Supplier">Qatar</option>
                                        <option value="Supplier">Kuwait</option>
                                        <option value="Supplier">Bahrain</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4"> Stored Place</label>
                                <div class="col-md-8 vendor-detail">
                                     <input type="text" class="form-control" name=stored_place" id="stored_place" placeholder="Stored Place" value="UK">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4"> Cabin</label>
                                <div class="col-md-8 vendor-detail">
                                    <input type="text" class="form-control" name=cabin" id="cabin" placeholder="Cabin" value="Cabin1">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4"> Min Level</label>
                                <div class="col-md-8 vendor-detail">
                                    <input type="text" class="form-control" name=min_level" id="min_level" placeholder="Min" value="15">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4"> Max Level</label>
                                <div class="col-md-8 vendor-detail">
                                    <input type="text" class="form-control" name=max_level" id="max_level" placeholder="Max" value="100">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4"> Reorder</label>
                                <div class="col-md-8 vendor-detail">
                                    <select class="form-control" name="reorder" id="reorder">
                                        <option value="">Select</option>
                                        <option value="Yes" selected>Yes</option>
                                        <option value="No">No</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                         <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4"> Image</label>
                                <div class="col-md-8 vendor-detail">
                                    <input type="file" class="form-control" name=item_image" id="item_image" value="">
                                    <p class="help-block">Note : (Upload Only .png,.jpg,.pdf)</p>
                                    <img src="assets/pages/img/iphone.jpg" style="width:100px;height:100px;">
                                </div>
                            </div>
                        </div>
                         <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                            <div class="form-group">
                                <label class="control-label col-md-4"> Status</label>
                                <div class="col-md-8 vendor-detail">
                                    <div class="mt-radio-inline">
                                    <label class="mt-radio">
                                    <input type="radio" name="optionsRadios" id="optionsRadios25" value="option1" checked>Enable
                                    <span></span>
                                    </label>
                                    <label class="mt-radio">
                                    <input type="radio" name="optionsRadios" id="optionsRadios26" value="option2"> Disable
                                    <span></span>
                                    </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                       
                       
                    </div>
                     <div class="modal-footer">
                        <button type="button" class="btn green customsavebtn" id="statuschange"> <i class="fa fa-check"></i> Save</button>
                        <button type="button" class="btn red customrestbtn"  data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
                    </div>
                </div>
            </form>
        <!-- /.modal-content -->
       </div>
    <!-- /.modal-dialog -->
   </div>
</div>

<?php 
include("footer.php"); 
?>


<script>
var i = 1;
$(document).on('click','#addmore', function() {
    var newfield ='<tr><td><input type="text" class="form-control" name="part_number[]" id="part_number" placeholder="Part Number"><span style="position: absolute;left: 133px; color: #4285f4; cursor: pointer;padding-right: 10px;" class="addpopup">Add New Part</span></td><td class="text-center" nowrap>   </td><td nowrap>   </td><td><input type="text" class="form-control" name="quantity[]" id="quantity" placeholder="Quantity" value="" style="width: 100px;"></td><td> <input type="text" class="form-control stock_date" name="stock_date[]" id="stock_date" placeholder="Date" value="" style="width: 100px;"></td><td><textarea class="form-control" name="remark[]" id="remark" placeholder="Remarks"></textarea></td><td><button type="button" class="btn btn-info customremovemorebtn" name="remove" id="remove"><span class="glyphicon glyphicon-remove" aria-hidden="true"></span></button></td></tr>';
    $('.appendContent').after(newfield);
    i++;
     $(".select2").select2({
      tags: true
    });
});
$(document).on('click','#remove', function(){
    $(this).parent().parent('tr').remove();
});
i++;
$(function() {
    $("#requisition_date,.stock_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});

$(document).ready(function() {
    $('.select2-hidden-accessible').select2();
});
$(document).ready(function () {
    $(".addpopup").click(function(){
         $('#itempop').modal('show');
    });
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
    $('.select2-hidden-accessible').select2();
    $('.select2').select2({width: '100%'});
});

  </script>