<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<style type="text/css">
    /*#mulsel{
            padding-top: 14px;
    padding-left: 30px;
    }*/
    .multiselect-container>li>a>label{padding: 3px 20px 3px 40px !important;}
    .btn-group>.dropdown-menu, .dropdown-toggle>.dropdown-menu, .dropdown>.dropdown-menu {
    margin-top: 10px;
    width: 100%;
}
</style>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/employee.png" class="imgbasline"> Add User</div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_user" id="frm_user" action="users_list.php?msg=1" class="form-horizontal" method="POST">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">First Name <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="first_name" id="first_name" placeholder="First Name" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Last Name <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="last_name" id="last_name" placeholder="Last Name" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Email <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Password <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="password" class="form-control" name="password" id="password" placeholder="Password" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Confirm Password <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="password" class="form-control" name="con_password" id="con_password" placeholder="Confirm Password" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Phone<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="phone" id="phone" placeholder="Phone" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Role<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control select2" name="role" id="role">
                                <option value="">Select Role</option>
                                <option value="1">Admin</option>
                                <option value="2">User</option>
                            </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="control-label col-md-3">Store<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control selectpicker" multiple data-actions-box="true">
                                <option value="Al Meera">Al Meera </option>
                                <option value=">Lulu Salmiya">Lulu Salmiya  </option>
                                <option value="Fine Line">Fine Line </option>
                                <option value="Classic">Classic</option>
                                <option value="Defence Co Op">Defence Co Op</option>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="control-label col-md-3">Apple Program<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control selectpicker" multiple data-actions-box="true">
                                <option value="Deployment">Deployment </option>
                                <option value=">Maintenance">Maintenance  </option>
                                <option value="BA merchandisin">BA merchandising </option>
                                <option value="Special projects">Special projects</option>
                                <option value="NPI">NPI</option>
                            </select>
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="control-label col-md-3"> Image <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                        <input type="file" class="form-control" name=item_image" id="item_image" value="">
                         <p class="help-block">(.png,.jpg,.pdf)</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Status<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="optionsRadios" id="optionsRadios25" value="option1" checked>Enable
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="optionsRadios" id="optionsRadios26" value="option2"> Disable
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn green customsavebtn">
                                <i class="fa fa-check"></i> Save
                            </button>
                            <a href="users_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>
<script type="text/javascript">
$(document).ready(function() {
    $('.select2-hidden-accessible').select2();
     $(".selectpicker").selectpicker({
        noneSelectedText : 'Select Apple Program' 
    });

    });
</script>
<script type="text/javascript">
$(document).ready(function() {
    $('#assembly').multiselect({includeSelectAllOption:true,buttonWidth: '323px',nonSelectedText: "Select Store",});
});
</script>