<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/return.png" class="imgbasline"> Stock Return List</div>
            <div class="actions">
                <a href="export_store_return.xls" class="btn green btn-sm excelbtn"><i class="fa fa-download"></i> Export to Excel</a>
               <a href="store_return.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Stock Return</a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12 paddingleftright">
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <select class="form-control select2" name="outlet_name" id="outlet_name">
                                <option value="">Select Outlet</option>
                                <option value="1">Axiom Delma Mall </option>
                                <option value="2">Al Thalla Cafeteria</option>
                                <option value="3">Ashjan Grocery</option>
                                <option value="4">Shafi Grocery</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <select class="form-control select2" name="supplier_name" id="supplier_name">
                                <option value="">Select Store</option>
                                <option value="1">Al Meera</option>
                                <option value="2">Lulu Salmiya</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="from_date" id="from_date" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="From Date">
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="to_date" id="to_date" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="To Date">
                        </div>
                    </div> 
                    <div class="col-md-12 text-center">
                        <div class="col-md-12 paddingleftright">
                            <button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
                            <a href="storereturn_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive" style="overflow-x: inherit;margin-top:15px;">
                <?php
                 if (strtolower($_SESSION["user_role"]) == "storeuser") {
                ?>
                <table class="table table-striped table-bordered table-hover" id="tblemployee">
                    <thead>
                        <tr>
                            <th> SI.NO </th>
                            <th> Outlet</th>
                            <th> Store</span></th>
                            <th> Date </th>
                            <th> Action </th>
                        </tr>
                    </thead>
                    </tbody>
                        <tr>
                            <td> 1 </td>
                            <td>Axiom Delma Mall</td>
                            <td> Al Meera - Warehouse </td>
                            <td> 21/01/2019 </td>
                            <td><a href="edit_storereturn.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a>  <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a></td>
                        </tr>
                         <tr>
                            <td> 2 </td>
                            <td> Al Thalla Cafeteria</td>
                            <td> Lulu Salmiya - Warehouse </td>
                            <td> 22/01/2019 </td>
                            <td> <a href="edit_storereturn.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a>  <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
                        </tr>
                         <tr>
                            <td> 3 </td>
                            <td> Ashjan Grocery </td>
                            <td>Al Meera - Warehouse </td>
                            <td> 23/01/2019 </td>
                            <td> <a href="edit_storereturn.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a>  <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
                        </tr>
                         <tr>
                            <td> 4 </td>
                            <td> Shafi Grocery</td>
                            <td> Lulu Salmiya - Maintenance </td>
                            <td> 24/01/2019 </td>
                            <td> <a href="edit_storereturn.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a>  <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
                        </tr>
                    </tbody>
                </table>
                <?php
                }
                ?>
                <?php
                 if (strtolower($_SESSION["user_role"]) == "maintenance") {
                ?>
                <table class="table table-striped table-bordered table-hover" id="tblrole">
                    <thead>
                        <tr>
                            <th> SI.NO </th>
                            <th> Return From <span style="display:block;">(Store)</span></th>
                            <th> Return To <span style="display:block;">(Store)</span></th>
                            <th> Date </th>
                            <th> Action </th>
                        </tr>
                    </thead>
                    </tbody>
                        <tr>
                            <td> 1 </td>
                            <td> Fine Line Mobile Phone - Maintenance </td>
                            <td> Al Meera- Deployment </td>
                            <td> 21/01/2019 </td>
                            <td> <a href="view_storereturn.php" type="button" class="btn grey-cascade btn-xs customeyebtn adddeliverynote"><i class="fa fa-eye"></i> View</a></td>
                        </tr>
                         <tr>
                            <td> 2 </td>
                            <td> Classic Mobile Phone - Deployment </td>
                            <td> Fine Line Mobile Phone - Maintenance </td>
                            <td> 22/01/2019 </td>
                            <td> <a href="edit_storereturn.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a>  <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
                        </tr>
                         <tr>
                            <td> 3 </td>
                            <td> Ibrar Shop - Maintenance </td>
                            <td> Fine Line Mobile Phone - Maintenance </td>
                            <td> 23/01/2019 </td>
                            <td> <a href="edit_storereturn.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a>  <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
                        </tr>
                         <tr>
                            <td> 4 </td>
                            <td> Fine Line Mobile Phone- Maintenance </td>
                            <td> Carrefour- Deployment </td>
                            <td> 24/01/2019 </td>
                            <td> <a href="view_storereturn.php" type="button" class="btn grey-cascade btn-xs customeyebtn adddeliverynote"><i class="fa fa-eye"></i> View</a></td>
                        </tr>
                    </tbody>
                </table>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>
<script>
    $(document).ready(function() {
    $('#tblrole,#tblemployee').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();
    $(document).ready(function() {
        $('.select2-hidden-accessible').select2();
    });
    $( function() {
      $("#from_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $( function() {
      $("#to_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
</script>