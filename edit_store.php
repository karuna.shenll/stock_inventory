<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/warehouse.png" class="imgbasline"> Edit Store</div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
           <form name="frm_item" id="frm_item" action="store_list.php?msg=1" class="form-horizontal" method="POST">
                <div class="form-body">
                     <div class="form-group">
                        <label class="control-label col-md-3"> Store Name <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                        <input type="text" class="form-control" name=store_name" id="store_name" placeholder="Store Name" value="Apple Store">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Apple Program <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control select2" name="store_type" id="store_type">
                                <option value="">Select Type</option>
                                <option value="Deployment" selected>Deployment </option>
                                <option value="Maintenance">Maintenance  </option>
                                <option value="BA merchandising">BA merchandising </option>
                                <option value="Special projects">Special projects</option>
                                <option value="NPI">NPI</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Store Code <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                        <input type="text" class="form-control" name=store_code" id="store_code" placeholder="Store Code" value="ST001">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Country <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                        <select class="form-control select2" name="store_country" id="store_country" >
                                <option value="Supplier">Select Country</option>
                                <option value="Supplier" selected>UAE </option>
                                <option value="Supplier">KSA_Riyadh </option>
                                <option value="Supplier">KSA_Jeddah </option>
                                <option value="Supplier">KSA_Dammam</option>
                                <option value="Supplier">Oman</option>
                                <option value="Supplier">Qatar</option>
                                <option value="Supplier">Kuwait</option>
                                <option value="Supplier">Bahrain</option>
                            </select>
                        </div>
                    </div>
                   <!--  <div class="form-group">
                        <label class="control-label col-md-3">Region <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                        <input type="text" class="form-control" name=region" id="region" placeholder="Region" value="">
                        </div>
                    </div> -->
                     <div class="form-group">
                        <label class="control-label col-md-3">City <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                        <input type="text" class="form-control" name=city" id="city" placeholder="City" value="UAE">
                        </div>
                    </div>
                    <!--  <div class="form-group">
                        <label class="control-label col-md-3">Channel <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                        <input type="text" class="form-control" name="channel" id="channel" placeholder="Channel" value="">
                        </div>
                    </div> -->
                     <div class="form-group">
                        <label class="control-label col-md-3">Account Name <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                        <input type="text" class="form-control" name=account_name" id="account_name" placeholder="Account Name" value="UAE Acc">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Address <span class="required" aria-required="true">  </span>
                        </label>
                        <div class="col-md-4">
                       <textarea id="address" name="address" class="form-control" placeholder="Address"> PO Box 17, Abu Dhabi, UAE</textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Status<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="optionsRadios" id="optionsRadios25" value="option1" checked>Enable
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="optionsRadios" id="optionsRadios26" value="option2"> Disable
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div> 
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn green customsavebtn">
                            <i class="fa fa-check"></i> Save
                            </button>
                            <a href="store_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Cancel</a>
                            </div>
                        </div>             
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>

<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>
<script type="text/javascript">
     $(document).ready(function() {
    $('.select2-hidden-accessible').select2();
    });
</script>