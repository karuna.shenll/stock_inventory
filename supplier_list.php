<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
if(isset($_GET["msg"])) {
    $msg  =	$_GET["msg"];
}
if($msg==1) {
	$message	=	"Supplier has been added successfully.";
} elseif($msg==2) {
	$message	=	"Supplier has been updated successfully.";
} elseif($msg==3) {
	$message	=	"Supplier has been deleted successfully.";
}
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
		if(!empty($message)) {
		?>
			<div class="alert alert-success">
					<a class="close" data-dismiss="alert" href="#">x</a>
		<?php echo $message;?>
			</div>
		<?php
		}
	?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/vendor.png" class="imgbasline"> Suppliers List</div>
            <div class="actions">
            	<a href="export_supplier_list.xls" class="btn green btn-sm excelbtn"><i class="fa fa-download"></i> Export to Excel</a>
                <a href="add_supplier.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Supplier</a>
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<select  class="form-control select2" name="supplier_id" id="supplier_id">
                                <option value=""> Select Supplier</option>
                                <option value="Supplier">Arvato</option>
                                <option value="Supplier">ARIAN</option>
                                <option value="Supplier">CCS DIGITAL</option>
                                <option value="Supplier">FUSION</option>
                                <option value="Supplier">SHIVAM</option>
                            </select>
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="email" id="email" placeholder="Email Address">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <select class="form-control select2" name="country" id="country" >
                                <option value="Supplier">Select Country</option>
                                <option value="Supplier">UAE </option>
                                <option value="Supplier">KSA_Riyadh </option>
                                <option value="Supplier">KSA_Jeddah </option>
                                <option value="Supplier">KSA_Dammam</option>
                                <option value="Supplier">Oman</option>
                                <option value="Supplier">Qatar</option>
                                <option value="Supplier">Kuwait</option>
                                <option value="Supplier">Bahrain</option>
                            </select>
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="city" id="city" placeholder="City">
                        </div>
	        		</div>
	        		<!-- <div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="region" id="region" placeholder="Region">
                        </div>
	        		</div> -->
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="accountname" id="accountname" placeholder="Account Name">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
	                        <select id="sel_status" name="sel_status" class="form-control">
	                        	<option value="">Select Status</option>
	                        	<option value="Enable">Enable</option>
	                        	<option value="Disable">Disable</option>
	                        </select>
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<a href="supplier_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover" id="tblvendor">
	            	<thead>
	                    <tr>
	                        <th nowrap> SI.NO </th>
	                       <!--  <th nowrap> Supplier Code </th> -->
	                        <th nowrap> Supplier Name </th>
	                        <th nowrap> Supplier Code </th>
	                        <th nowrap> Email </th>
	                        <th nowrap> Country </th>
	                        <th nowrap> City </th>
	                       <!--  <th nowrap> Region </th> -->
	                        <th nowrap> Account Name</th>
	                        <th nowrap> Status </th>
	                        <th nowrap> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                        <td nowrap> 1 </td>
	                        <!-- <td> 10609 </td>  -->
	                        <td nowrap>Arvato </td>
	                        <td nowrap> ST001 </td>
	                        <td nowrap> abb@gmail.com</td>
	                        <td nowrap> UAE</td>
	                        <td nowrap> Abu Dhabi</td>
	                        <!-- <td nowrap> Abu Dhabi</td> -->
	                        <td nowrap>Direct Retail </td>
	                        <td nowrap><span class="label label-sm label-success labelboader"> Enable </span> </td>
	                        <td nowrap> <a href="edit_supplier.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td nowrap> 2 </td>
	                        <!-- <td nowrap> 13423 </td> -->
	                        <td nowrap> ARIAN </td>
	                        <td nowrap> ST002 </td>
	                        <td nowrap> ritestar.tech@gmail.com</td>
	                        <td nowrap> KSA_Riyadh</td>
	                        <td nowrap> Ajman </td>
	                       <!--  <td nowrap> Ajman </td> -->
	                        <td nowrap>du Express Indirect </td>
	                        <td nowrap><span class="label label-sm label-danger labelboader"> Disable </span> </td>
	                        <td nowrap> <a href="edit_supplier.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td nowrap> 3 </td>
	                       <!--  <td> 28200 </td> -->
	                        <td nowrap> CCS DIGITAL </td>
	                        <td nowrap> ST003 </td>
	                        <td nowrap> helukabel@gmail.com</td>
	                        <td nowrap> Oman</td>
	                        <td nowrap> Oman</td>
	                       <!--  <td nowrap> Sharjah</td> -->
	                        <td nowrap>Direct Retail-4</td>
	                        <td nowrap><span class="label label-sm label-success labelboader"> Enable </span> </td>
	                        <td nowrap> <a href="edit_supplier.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td nowrap> 4 </td>
	                        <!-- <td nowrap> 13219 </td> -->
	                        <td nowrap> FUSION  </td>
	                        <td nowrap> ST004 </td>
	                        <td nowrap> fusion@gmail.com</td>
	                        <td nowrap> Qatar</td>
	                        <td nowrap> Qatar</td>
	                        <!-- <td nowrap> Al Ain</td> -->
	                        <td nowrap>Mass Market Quality Outlets </td>
	                        <td><span class="label label-sm label-danger labelboader"> Disable </span> </td>
	                        <td> <a href="edit_supplier.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td nowrap> 5 </td>
	                        <!-- <td nowrap> 13133 </td> -->
	                        <td nowrap> SHIVAM  </td>
	                        <td nowrap> ST005 </td>
	                        <td nowrap> shivameng@gmail.com</td>
	                        <td nowrap> Bahrain</td>
	                        <td nowrap> Bahrain</td>
	                        <!-- <td nowrap> Fujairah</td> -->
	                        <td nowrap>Direct Retail </td>
	                        <td nowrap><span class="label label-sm label-success labelboader"> Enable </span> </td>
	                        <td nowrap> <a href="edit_supplier.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
	$('#tblvendor').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();

     $(document).ready(function() {
    $('.select2-hidden-accessible').select2();
    });
</script>