<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/disposal.png" class="imgbasline"> Items Disposal/Destruction List</div>
            <div class="actions">
                <a href="download.xls" class="btn green btn-sm excelbtn"><i class="fa fa-download"></i> Export to Excel</a>
               <!--  <a href="add_stocktransfer.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Stock Issuance / Transfers</a> -->
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12 paddingleftright">
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <select class="form-control select2" name="supplier_name" id="supplier_name">
                                <option value="">Select Store</option>
                                <option value="1">Al Meera</option>
                                <option value="2">Lulu Salmiya</option>
                                <option value="3">Defence Co Op</option>
                                <option value="4">Carrefour</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <input type="text" class="form-control" name="transfers_id" id="transfers_id" placeholder="Transfers Id">
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="from_date" id="from_date" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="From Date">
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="to_date" id="to_date" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="To Date">
                        </div>
                    </div> 
                    <div class="col-md-12 text-center">
                        <div class="col-md-12 paddingleftright">
                            <button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
                            <a href="storedisposal_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive" style="overflow-x: inherit;margin-top:15px;">
                <table class="table table-striped table-bordered table-hover" id="tblemployee">
                    <thead>
                        <tr>
                            <th> SI.NO </th>
                            <th> Transfers Id </th>
                            <th> Store Code </th>
                            <th> Store Name </th>
                            <th> Disposal Date </th>
                            <th> Action </th>
                        </tr>
                    </thead>
                    </tbody>
                        <tr>
                            <td> 1 </td>
                            <td> TRID7001 </td>
                            <td> 1001 </td>
                            <td> Al Meera </td>
                            <td> 21/01/2019 </td>
                            <td> <a href="store_disposal.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a>  <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
                        </tr>
                         <tr>
                            <td> 2 </td>
                            <td> TRID7002 </td>
                            <td> 1002 </td>
                            <td> Lulu Salmiya </td>
                            <td> 22/01/2019 </td>
                            <td> <a href="store_disposal.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a>  <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
                        </tr>
                         <tr>
                            <td> 3 </td>
                            <td> TRID7003 </td>
                            <td> 1003 </td>
                            <td> Defence Co Op </td>
                            <td> 23/01/2019 </td>
                            <td> <a href="store_disposal.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a>  <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
                        </tr>
                         <tr>
                            <td> 4 </td>
                            <td> TRID7004 </td>
                            <td> 1004 </td>
                            <td> Carrefour </td>
                            <td> 24/01/2019 </td>
                            <td> <a href="store_disposal.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a>  <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>
<script>
    $(document).ready(function() {
    $('#tblrole').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();
    $(document).ready(function() {
        $('.select2-hidden-accessible').select2();
    });
    $( function() {
      $("#from_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $( function() {
      $("#to_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
</script>