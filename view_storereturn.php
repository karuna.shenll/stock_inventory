<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/return.png" class="imgbasline"> View Stock Return</div>
            <div class="actions">
                <a href="storereturn_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-angle-left"></i> Back</a>
            </div>
        </div>
        <div class="portlet-body">
            <form name="frm_data" id="frm_data" action="storereturn_list.php" class="form-horizontal" method="POST">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Store Name <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                           <input type="text" class="form-control" name="store_name" id="store_name" placeholder="Store Name"  value="Classic Mobile Phone" disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Return Date <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="return_date" id="return_date" placeholder="Return Date" autocomplete="off" data-date-format="dd/mm/yyyy" value="28/01/2019" disabled>
                        </div>
                    </div>
                    <div class="storereturndisp" style="display:block">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="tblstorerecipt">
                                        <thead>
                                            <tr>
                                                <!-- <th nowrap>Item</th> -->
                                                <th nowrap class="text-center">Part Number</th>
                                                <th nowrap>Description</th>
                                                <th nowrap class="text-center">Apple ID</th>
                                                <th nowrap class="text-center">Quantity</th>
                                                <th nowrap class="text-center">Date</th>
                                                <th nowrap>Condition</th>
                                                <th nowrap>Remarks</th>
                                                <th nowrap class="text-center">Image</th>
                                                <th nowrap>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                               <!--  <td nowrap>Apple TV Base Unit</td> -->
                                                <td nowrap class="text-center">622-00084</td>
                                                <td nowrap>Apple TV Base Unit</td>
                                                <td nowrap class="text-center">1093880</td>
                                                <td class="text-center">2</td>
                                                <td class="text-center">29/01/2019</td>
                                                <td> Normal</td>
                                                <td>
                                                   QATAR APR SPARES
                                                </td>
                                                <td nowrap class="text-center">
                                                    <div class="image-upload">
                                                        <label for="imgupload">
                                                            <img src="assets/pages/img/upload-icon.png"/>
                                                        </label>
                                                        <input type="file" class="form-control" name="imgupload[]" id="imgupload"/>
                                                    </div>
                                                    <a href="javascript:void(0);" class="imagepopup"> <span>3</span></a>
                                                </td>
                                                <td>
                                                    Damaged
                                                       
                                                </td>
                                            </tr>
                                            <tr>
                                                <!-- <td nowrap >Apple TV Cable Clip</td> -->
                                                <td nowrap class="text-center">677-01423</td>
                                                <td nowrap>Apple TV Cable Clip</td>
                                                <td nowrap class="text-center">1599662</td>
                                                <td class="text-center">5</td>
                                                <td class="text-center">30/01/2019</td>
                                                <td>Good</td>
                                                <td>
                                                    virgin megastore villagio
                                                </td>
                                                <td nowrap class="text-center">
                                                    <div class="image-upload">
                                                        <label for="imgupload">
                                                            <img src="assets/pages/img/upload-icon.png"/>
                                                        </label>
                                                        <input type="file" class="form-control" name="imgupload[]" id="imgupload"/>
                                                    </div>
                                                    <a href="javascript:void(0);" class="imagepopup"> <span>2</span></a>
                                                </td>
                                                <td>
                                                   Broken
                                                </td>
                                            </tr>
                                            <tr>
                                                <!-- <td nowrap >USBC-USBA Cable</td> -->
                                                <td nowrap class="text-center">622-00072</td>
                                                <td nowrap>USBC-USBA Cable</td>
                                                <td nowrap class="text-center">XXXXQATAPR</td>
                                                <td class="text-center">4</td>
                                                <td class="text-center">31/01/2019</td>
                                                <td>Normal</td>
                                                <td>
                                                   QATAR APR SPARES
                                                </td>
                                                <td nowrap class="text-center">
                                                    <div class="image-upload">
                                                        <label for="imgupload">
                                                            <img src="assets/pages/img/upload-icon.png"/>
                                                        </label>
                                                        <input type="file" class="form-control" name="imgupload[]" id="imgupload"/>
                                                    </div>
                                                   <a href="javascript:void(0);" class="imagepopup"> <span>1</span></a>
                                                </td>
                                                <td>
                                                    Damaged
                                                </td>
                                            </tr>
                                            <tr>
                                                <!-- <td nowrap >SR IPAD PRO WARM GPF24</td> -->
                                                <td nowrap class="text-center">622-00119</td>
                                                <td nowrap>SR IPAD PRO WARM GPF24</td>
                                                <td nowrap class="text-center">XXXXQATWHITE</td>
                                                <td class="text-center">6</td>
                                                <td class="text-center">01/02/2019</td>
                                                <td> good</td>
                                                <td>
                                                    virgin megastore villagio
                                                </td>
                                                <td nowrap class="text-center">
                                                    <div class="image-upload">
                                                        <label for="imgupload">
                                                            <img src="assets/pages/img/upload-icon.png"/>
                                                        </label>
                                                        <input type="file" class="form-control" name="imgupload[]" id="imgupload"/>
                                                    </div>
                                                   <a href="javascript:void(0);" class="imagepopup"> <span>4</span></a>
                                                </td>
                                                <td>
                                                   Broken
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <a href="storereturn_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-angle-left"></i> Back</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade in" id="imagePopup" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><b>View Image</b></h4>
            </div>
            <div class="modal-body"> 
                <div class="row">
                    <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <img src="assets/pages/img/iphone.jpg" class="img-responsive" style="border:1px solid #ddd;">
                                </div>
                                <div class="col-md-4">
                                    <img src="assets/pages/img/phone.png" class="img-responsive" style="border:1px solid #ddd;">
                                </div>
                                <div class="col-md-4">
                                   <img src="assets/pages/img/xperia.png" class="img-responsive" style="border:1px solid #ddd;">
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn red customrestbtn"  data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>
<script>

     $(document).ready(function() {
    $('.select2-hidden-accessible').select2();
    });
     $(function() {
    $("#return_date,.stock_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});
 $(document).ready(function () {
        $(".imagepopup").click(function(){
             $('#imagePopup').modal('show');
        });
    });
         
</script>