<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/upload.png" class="imgbasline"> View Stock</div>
            <div class="actions">
             <a href="storereceipt_list.php" class="btn red customrestbtn" id="resetEmpty"> < Back</a>
            </div>
        </div>
        <div class="portlet-body">
           <form name="frm_data" id="frm_data" action="storereceipt_list.php" class="form-horizontal" method="POST">
                <div class="form-body">
                    
                    <div class="form-group">
                        <label class="control-label col-md-3">Store Name <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                           <input type="text" class="form-control" name="waybill" id="waybill" placeholder="Waybill" value="Al Meera" readonly="" >
                        </div>
                    </div>
                    <!-- <div class="form-group">
                        <label class="control-label col-md-3">Waybill <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="waybill" id="waybill" placeholder="Waybill" value="1Z7488EE04522244" readonly="" >
                        </div>
                    </div> -->
                    <div class="form-group">
                        <label class="control-label col-md-3">Received Date <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="requisition_date" id="requisition_date" placeholder="Received Date" autocomplete="off" data-date-format="dd/mm/yyyy" value="29/01/2019" readonly="">
                        </div>
                    </div>
                    <h3 class="form-section formheading">Stock In</h3>
                    <div class="col-md-12">
                        <div class="row">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="tblstorerecipt">
                                    <thead>
                                        <tr>
                                            <th class="text-center">Part Number</th>
                                            <th class="text-center">Apple ID</th>
                                            <th>Description</th>
                                            <th>Quantity</th>
                                            <th>Date</th>
                                            <th>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                             <td>
                                               622-00084
                                            </td>
                                            <td nowrap>Apple TV Base Unit</td>
                                            <td class="text-center" nowrap> 1093880</td>                                           
                                            <td>
                                                4
                                            </td>
                                            <td> 29/01/2019
                                            <td>
                                               QATAR APR SPARES
                                           </td>
                                        </tr>
                                        <tr>
                                            <td>

                                                677-01423
                                            </td>
                                            <td nowrap> Apple TV Cable Clip</td>
                                            <td class="text-center" nowrap>1599662</td>
                                            
                                            <td>
                                                3
                                            </td>
                                            <td>
                                               30/01/2019
                                            </td>
                                            <td>
                                               IQ VILLAGIO MALL
                                            </td>
                                        </tr>
                                        <tr>
                                             <td>
                                               622-00084
                                            </td>
                                            <td nowrap>USBC-USBA Cable</td>
                                            <td class="text-center" nowrap>XXXXQATAPR</td>
                                            
                                            <td> 8 </td>
                                            <td> 31/01/2019</td>
                                            <td>
                                               QATAR MOBILITY SPARES
                                            </td>
                                        </tr>
                                        <tr class="appendContent">
                                            <td>
                                            GC24806A-MEAR
                                            </td>
                                            <td nowrap> SR IPAD PRO WARM GPF24</td>
                                            <td class="text-center" nowrap> XXXXQATWHITE</td>
                                            
                                            <td>6</td>
                                            <td> 01/02/2019</td>
                                            <td>
                                              QATAR APPLE SHOP 2.0 SPARES
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12 text-right">
                           <!--  <button type="submit" class="btn green customsavebtn">
                                <i class="fa fa-check"></i> Save
                            </button> -->
                            <a href="storereceipt_list.php" class="btn red customrestbtn" id="resetEmpty"> < Back</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>


