<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
if(isset($_GET["msg"])) {
    $msg  =	$_GET["msg"];
}
if($msg==1) {
	$message	=	"Item has been added successfully.";
} elseif($msg==2) {
	$message	=	"Item has been updated successfully.";
} elseif($msg==3) {
	$message	=	"Item has been deleted successfully.";
}
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	<?php
		if(!empty($message)) {
		?>
			<div class="alert alert-success">
					<a class="close" data-dismiss="alert" href="#">x</a>
		<?php echo $message;?>
			</div>
		<?php
		}
	?>
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/store.png" class="imgbasline"> Item List </div>
            <div class="actions">
            	<a href="export_items_list.xls" class="btn green btn-sm excelbtn"><i class="fa fa-download"></i> Export to Excel</a>
                <a href="add_item.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Item </a>
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="part_number" id="part_number" placeholder="Part Number">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="part_des" id="part_des" placeholder="Part Description">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="apple_id" id="apple_id" placeholder="AppleID">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<select class="form-control select2" name="supplier_id" id="supplier_id" >
                        		<option value="">Select Supplier</option>
                        		<option value="Supplier">Arvato</option>
                        		<option value="Supplier">ARIAN</option>
                        		<option value="Supplier">CCS DIGITAL</option>
                        		<option value="Supplier">FUSION</option>
                        	</select>
                        </div>
	        		</div>
	        		<!-- <div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<select class="form-control select2" name="store_name" id="store_name" >
                                <option value="Supplier">Select Country</option>
                                <option value="Supplier">UAE </option>
                                <option value="Supplier">KSA_Riyadh </option>
                                <option value="Supplier">KSA_Jeddah </option>
                                <option value="Supplier">KSA_Dammam</option>
                                <option value="Supplier">Oman</option>
                                <option value="Supplier">Qatar</option>
                                <option value="Supplier">Kuwait</option>
                                <option value="Supplier">Bahrain</option>
                            </select>
                        </div>
	        		</div> -->
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
	                        <select id="sel_status" name="sel_status" class="form-control">
	                        	<option value="">Select Status</option>
	                        	<option value="Enable">Enable</option>
	                        	<option value="Disable">Disable</option>
	                        </select>
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<a href="item_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover" id="tblrole">
	            	<thead>
	                    <tr>
	                        <th> SI.NO </th>
	                        <th> Part Number </th>
	                        <th> Part Description </th>
	                        <th> Apple ID </th>
	                        <th> Supplier Name </th>
	                        <th> Status </th>
	                        <th> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                        <td> 1 </td>
	                        <td> 818-02365</td>
	                        <td> IOS Dock Adhesive removal tool</td>
	                        <td> 546881 </td>
	                        <td> Arvato </td>
	                        <td><span class="label label-sm label-success labelboader"> Enable </span> </td>
	                        <td> <a href="edit_item.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 2 </td>
	                        <td> 622-00119 </td>
	                         <td>Security Cable : Smart Keyboard </td>
	                        <td> 1093880 </td>
	                        <td> ARIAN </td>
	                        <td><span class="label label-sm label-danger labelboader"> Disable </span> </td>
	                        <td> <a href="edit_item.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td> 3 </td>
	                        <td> 677-03900 </td>
	                        <td> Apple TV Base Unit</td>
	                        <td> 1599662 </td>
	                        <td> CCS DIGITAL </td>
	                        <td><span class="label label-sm label-success labelboader"> Enable </span> </td>
	                        <td> <a href="edit_item.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                     <tr>
	                        <td> 4 </td>
	                        <td> 677-03901 </td>
	                        <td> Apple TV Base Kit</td>
	                        <td> 1599661 </td>
	                        <td> FUSION </td>
	                        <td><span class="label label-sm label-danger labelboader"> Disable </span> </td>
	                        <td> <a href="edit_item.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
	$('#tblrole').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();

    $(document).ready(function() {
    $('.select2-hidden-accessible').select2();
    });
</script>