<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/area.png" class="imgbasline"> Edit Sub Category</div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_subcategory" id="frm_subcategory" action="subcategory_list.php?msg=2" class="form-horizontal" method="POST">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Category <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control" name="category_id" id="category_id" >
                                <option value="">Select Category</option>
                                <option value="Electronics" selected>Iphone</option>
                                <option value="Home Appliance">Apple TV</option>
                                <option value="Food Products">IPAD</option>
                                <option value="Stationary products">Mac Book</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Sub Category <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                        <input type="text" class="form-control" name=sub_category_name" id="sub_category_name" placeholder="Sub Category" value="Mobile Phone">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Status<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="optionsRadios" id="optionsRadios25" value="option1" checked>Enable
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="optionsRadios" id="optionsRadios26" value="option2"> Disable
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                 <div class="form-actions">
                        <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                        </button>
                        <a href="subcategory_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Cancel</a>
                        </div>
                        </div>
                    </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>