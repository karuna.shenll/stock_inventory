<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/upload.png" class="imgbasline"> Stock In List</div>
            <div class="actions">
                 <a href="javascript:void(0);" class="btn green btn-sm customviewbtn bulkpopup"><i class="fa fa-file-excel-o"></i> Bulk Import</a>
                <a href="export_store_receipt.xls" class="btn green btn-sm excelbtn"><i class="fa fa-download"></i> Export to Excel</a>
                <a href="add_storereceipt.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Stock</a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12 paddingleftright">
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <select class="form-control select2" name="supplier_name" id="supplier_name">
                                <option value="">Select Manufacturer</option>
                                <option value="1">Arvato</option>
                                <option value="2">RHEIM</option>
                                <option value="3">Defence Co Op</option>
                                <option value="4">Fusion</option>
                            </select>
                        </div>
                    </div>
                     <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="waybill" id="waybill"  placeholder="Waybill">
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="from_date" id="from_date" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="From Date">
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="to_date" id="to_date" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="To Date">
                        </div>
                    </div> 
                    <div class="col-md-12 text-center">
                        <div class="col-md-12 paddingleftright">
                            <button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
                            <a href="storereceipt_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive" style="overflow-x: inherit;margin-top:15px;">
                <table class="table table-striped table-bordered table-hover" id="tblemployee">
                    <thead>
                        <tr>
                            <th> SI.NO </th>
                            <th> Manufacturer </th>
                            <th> Waybill </th>
                            <th> Received Date </th>
                            <th> Action </th>
                        </tr>
                    </thead>
                    </tbody>
                        <tr>
                            <td> 1 </td>
                            <td> Arvato </td>
                            <td> 1Z7488EE0452224476 </td>
                            <td> 21/01/2019 </td>
                            <td> <a href="edit_storereceipt.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a>  <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
                        </tr>
                         <tr>
                            <td> 2 </td>
                            <td> RHEIM </td>
                            <td> 1Z7488EE0452224454 </td>
                            <td> 22/01/2019 </td>
                            <td> <a href="edit_storereceipt.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a>  <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
                        </tr>
                         <tr>
                            <td> 3 </td>
                            <td> Defence Co Op </td>
                            <td> 1Z7488EE04522243434 </td>
                            <td> 23/01/2019 </td>
                            <td> <a href="edit_storereceipt.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a>  <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
                        </tr>
                         <tr>
                            <td> 4 </td>
                            <td> FUSION </td>
                            <td> 1Z7488EE04522244423 </td>
                            <td> 24/01/2019 </td>
                            <td> <a href="edit_storereceipt.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a>  <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<div class="modal fade in" id="receiptpop" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><b>Bulk Import of Receipts</b></h4>
            </div>
            <div class="modal-body"> 
                <div class="row">
                    <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="control-label col-md-3">Bulk Import</label>
                                <div class="col-md-9">
                                    <input type="file" class="form-control" name="bulk_import" id="bulk_import">
                                    <p class="help-block" style="text-align: left;"><i>Note: (Upload only .xls file format)</i></p>
                                     <a href="receipt_format.xls" style="display: inline-block;text-decoration: none;font-size:12.5px;" href="../assets/layouts/layout/img/avatar3_small.jpg"><img src="assets/pages/img/download.png"> Click Here to Download Bulk Upload Receipt Format</a><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn green customsavebtn" data-dismiss="modal"> <i class="fa fa-check"></i> Bulk Import</button>
                <button type="button" class="btn red customrestbtn"  data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<?php 
include("footer.php"); 
?>
<script>
    $(document).ready(function() {
    $('#tblrole').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();
    $(document).ready(function() {
        $('.select2-hidden-accessible').select2();
    });
    $( function() {
      $("#from_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $( function() {
      $("#to_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });


  $(document).ready(function () {
    $(".bulkpopup").click(function(){
         $('#receiptpop').modal('show');
    });
});
</script>