<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/edit-form.png" class="imgbasline"> Add Stock Issuance</div>
            <div class="actions">
                <!-- <a href="add_sub_subcategory.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Sub Sub-Category</a> -->
            </div>
        </div>
        <div class="portlet-body">
            <form name="frm_data" id="frm_data" action="stockissuance_list.php" class="form-horizontal" method="POST">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Store<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select  class="form-control select2" name="supplier_code" id="supplier_code">
                                <option value="">Select Store</option>
                                <option value="1" selected="">Al Meera</option>
                                <option value="2">Lulu Salmiya</option>
                                
                            </select>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Outlet<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control select2" name="outlet_name" id="outlet_name">
                                <option value="">Select Outlet</option>
                                <option value="1" selected>Axiom Delma Mall </option>
                                <option value="2">Al Thalla Cafeteria</option>
                                <option value="3">Ashjan Grocery</option>
                                <option value="4">Shafi Grocery</option>
                            </select>
                        </div>
                    </div>
                   
                    <div class="form-group">
                        <label class="control-label col-md-3">Issuance Date <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="issued_date" id="issued_date" placeholder="Issuance Date" autocomplete="off" data-date-format="dd/mm/yyyy" value="28/01/2019">
                        </div>
                    </div>
                    <h3 class="form-section formheading displaytransferitem">Items</h3>
                    <div class="row">
                        <div class="col-md-12 displaytransferitem">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="tblstorerecipt">
                                    <thead>
                                        <tr>
                                           <!--  <th nowrap>Item</th> -->
                                            <th nowrap>Part Number</th>
                                            <th nowrap>Description</th>
                                            <th nowrap class="text-center">Apple ID</th>
                                            <th nowrap>Quantity</th>
                                            <th nowrap>Date</th>
                                            <th nowrap>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <!-- <td nowrap>Apple TV Base Unit</td> -->
                                            <td nowrap>  <input type="text" class="form-control" name="part_number[]" id="part_number" placeholder="Part Number" value="622-00084"></td>
                                            <td nowrap>Apple TV Base Unit</td>
                                            <td class="text-center">1093880</td>
                                            <td>
                                                <input type="text" class="form-control" name="quantity[]" id="quantity" placeholder="Quantity" value="5" style="width: 100px;">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control stock_date" name="stock_date[]" id="stock_date" placeholder="Date" value="30/01/2019" style="width: 100px;">
                                            </td>
                                            <td nowrap> 
                                                <textarea type="text" class="form-control" name="remarks[]" id="remarks" placeholder="Remarks" style="width: 200px !important;">QATAR APR SPARES</textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td nowrap>  <input type="text" class="form-control" name="part_number[]" id="part_number" placeholder="Part Number" value="677-01423"></td>
                                            <td nowrap>Apple TV Cable Clip</td>
                                            <td class="text-center">1599662</td>
                                            <td>
                                                <input type="text" class="form-control" name="quantity[]" id="quantity" placeholder="Quantity" value="7" style="width: 100px;">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control stock_date" name="stock_date[]" id="stock_date" placeholder="Date" value="31/01/2019" style="width: 100px;">
                                            </td>
                                            <td nowrap> 
                                                <textarea type="text" class="form-control" name="remarks[]" id="remarks" placeholder="Remarks" style="width: 200px !important;">QATAR APPLE SHOP 2.0 SPARES</textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                           <td nowrap>  <input type="text" class="form-control" name="part_number[]" id="part_number" placeholder="Part Number" value="622-00084"></td>
                                            <td nowrap>USBC-USBA Cable</td>
                                            <td class="text-center">XXXXQATAPR</td>
                                            <td>
                                                <input type="text" class="form-control" name="quantity[]" id="quantity" placeholder="Quantity" value="3" style="width: 100px;">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control stock_date" name="stock_date[]" id="stock_date" placeholder="Date" value="01/02/2019" style="width: 100px;">
                                            </td>
                                            <td nowrap> 
                                                <textarea type="text" class="form-control" name="remarks[]" id="remarks" placeholder="Remarks" style="width: 200px !important;">QATAR APR SPARES</textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td nowrap>  <input type="text" class="form-control" name="part_number[]" id="part_number" placeholder="Part Number" value="GC24806A-MEAR"></td>
                                            <td nowrap>SR IPAD PRO WARM GPF24</td>
                                            <td class="text-center">XXXXQATWHITE</td>
                                            <td>
                                                <input type="text" class="form-control" name="quantity[]" id="quantity" placeholder="Quantity" value="10" style="width: 100px;">
                                            </td>
                                            <td>
                                                <input type="text" class="form-control stock_date" name="stock_date[]" id="stock_date" placeholder="Date" value="01/02/2019" style="width: 100px;">
                                            </td>
                                            <td nowrap> 
                                                <textarea type="text" class="form-control" name="remarks[]" id="remarks" placeholder="Remarks" style="width: 200px !important;">QATAR WHITE SPARES</textarea>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                         </div>
                    </div>
                </div>
                <div class="form-actions  displaytransferitem">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <button type="submit" class="btn green customsavebtn">
                                <i class="fa fa-check"></i> Save
                            </button>
                            <a href="stockissuance_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
    // For edit page hide
    //var purchase = $('#purchase_code option:selected').text();
    //alert("yeyyeyyw");
    // if (purchase!="") {
    //      $(".displaytransferitem").css("display","block");
    // } else {
    //     $(".displaytransferitem").css("display","none");
    // }
// $(document).on("change","#purchase_code",function(){
//     var purchase_code=$(this).val();
//     if(purchase_code!=""){
//         $(".displaytransferitem").css("display","block");
//     } else {
//         $(".displaytransferitem").css("display","none");
//     }

// });
});
     $(document).ready(function() {
    $('.select2-hidden-accessible').select2();
    });
     $(function() {
    $("#issued_date,.stock_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});
</script>