<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/store.png" class="imgbasline"> Add Item</div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_item" id="frm_item" action="item_list.php?msg=1" class="form-horizontal" method="POST">
                <div class="form-body">
                   
                    <div class="form-group">
                        <label class="control-label col-md-3">Part Number <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                        <input type="text" class="form-control" name="s_no" id="s_no" placeholder="Part Number" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Part Description <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                        <textarea id="part_des" name="part_des" placeholder="Part Description" class="form-control"></textarea>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Apple ID <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                        <input type="text" class="form-control" name="apple_id" id="apple_id" placeholder="Apple ID" value="">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Supplier Name <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control select2" name="supplier_id" id="supplier_id" >
                                <option value=""> Select Supplier</option>
                                <option value="Supplier">Arvato</option>
                                <option value="Supplier">ARIAN</option>
                                <option value="Supplier">CCS DIGITAL</option>
                                <option value="Supplier">FUSION</option>
                            </select>
                        </div>
                    </div>

                     <div class="form-group">
                        <label class="control-label col-md-3">Country <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control select2" name="store_name" id="store_name" >
                                <option value="Supplier">Select Country</option>
                                <option value="Supplier">UAE </option>
                                <option value="Supplier">KSA_Riyadh </option>
                                <option value="Supplier">KSA_Jeddah </option>
                                <option value="Supplier">KSA_Dammam</option>
                                <option value="Supplier">Oman</option>
                                <option value="Supplier">Qatar</option>
                                <option value="Supplier">Kuwait</option>
                                <option value="Supplier">Bahrain</option>
                            </select>
                        </div>
                    </div>
                  <!--   <div class="form-group">
                        <label class="control-label col-md-3">Category <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                           <select class="form-control select2" name="sub_subcategory_id" id="sub_subcategory_id" >
                                <option value="">Select Category</option>
                                <option value="Iphone">Iphone</option>
                                <option value="Apple TV">Apple TV</option>
                                <option value="IPAD">IPAD</option>
                                <option value="Mac Book">Mac Book</option>
                            </select>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Sub Category <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                           <select class="form-control select2" name="sub_subcategory_id" id="sub_subcategory_id" >
                                <option value="">Select Sub Category</option>
                                <option value="Iphone X">Iphone X</option>
                                <option value="Apple TV v1">Apple TV v1</option>
                                <option value="Ipad V1">Ipad V1</option>
                                <option value="Mackbook v2">Mackbook v2</option>
                            </select>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Sub Sub-Category <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                           <select class="form-control select2" name="sub_subcategory_id" id="sub_subcategory_id" >
                                <option value="">Select Sub Sub-Category</option>
                                <option value="Head Phone">Head Phone</option>
                                <option value="Laptop">Display</option>
                                <option value="Pencil">Cable</option>
                                <option value="Food Products">Wall Panel</option>
                            </select>
                        </div>
                    </div> -->
                     <div class="form-group">
                        <label class="control-label col-md-3">Stored Place <span class="required" aria-required="true">  </span>
                        </label>
                        <div class="col-md-4">
                        <input type="text" class="form-control" name=stored_place" id="stored_place" placeholder="Stored Place" value="">
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Cabin <span class="required" aria-required="true">  </span>
                        </label>
                        <div class="col-md-4">
                        <input type="text" class="form-control" name=cabin" id="cabin" placeholder="Cabin" value="">
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Min Level <span class="required" aria-required="true">  </span>
                        </label>
                        <div class="col-md-4">
                        <input type="text" class="form-control" name=min_level" id="min_level" placeholder="Min" value="">
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Max Level <span class="required" aria-required="true">  </span>
                        </label>
                        <div class="col-md-4">
                        <input type="text" class="form-control" name=max_level" id="max_level" placeholder="Max" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Reorder <span class="required" aria-required="true">  </span>
                        </label>
                        <div class="col-md-4">
                           <select class="form-control" name="reorder" id="reorder">
                                <option value="">Select</option>
                                <option value="Yes">Yes</option>
                                <option value="No">No</option>
                            </select>
                        </div>
                    </div>
                   <!--  <div class="form-group">
                        <label class="control-label col-md-3">Qty <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                        <input type="text" class="form-control" name=qty" id="qty" placeholder="Qty" value="">
                        </div>
                    </div> -->
                     <div class="form-group">
                        <label class="control-label col-md-3"> Image <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                        <input type="file" class="form-control" name=item_image" id="item_image" value="">
                         <p class="help-block">Note : (Upload Only .png,.jpg,.pdf)</p>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Status<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <div class="mt-radio-inline">
                                <label class="mt-radio">
                                    <input type="radio" name="optionsRadios" id="optionsRadios25" value="option1" checked>Enable
                                    <span></span>
                                </label>
                                <label class="mt-radio">
                                    <input type="radio" name="optionsRadios" id="optionsRadios26" value="option2"> Disable
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>              
                </div>
                 <div class="form-actions">
                        <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                        <button type="submit" class="btn green customsavebtn">
                        <i class="fa fa-check"></i> Save
                        </button>
                        <a href="item_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Cancel</a>
                        </div>
                        </div>
                    </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>

<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>
<script type="text/javascript">
     $(document).ready(function() {
    $('.select2-hidden-accessible').select2();
    });
</script>