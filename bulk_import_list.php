<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/vendor.png" class="imgbasline"> Bulk Import List</div>
            <div class="actions">
                <a href="add_bulk_import.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Bulk Import</a>
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	 <select class="form-control select2" name="supplier_id" id="supplier_id" >
                                <option value=""> Select Supplier</option>
                                <option value="Supplier 1">Supplier 1</option>
                                <option value="Supplier 2">Supplier 2</option>
                                <option value="Supplier 3">Supplier 3</option>
                                <option value="Supplier 4">Supplier 4</option>
                            </select>
                        </div>
	        		</div>
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<a href="supplier_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover" id="tblvendor">
	            	<thead>
	                    <tr>
	                        <th nowrap> SI.NO </th>
	                        <th nowrap> Supplier Code </th>
	                        <th nowrap> Supplier Name </th>
	                        <th nowrap> Action </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                        <td nowrap> 1 </td>
	                        <td> 10609 </td> 
	                        <td nowrap> ABB LTD </td>	                       
	                        <td nowrap> <a href="edit_bulk_import.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td nowrap> 2 </td>
	                        <td nowrap> 13423 </td>
	                        <td nowrap> RITESTAR </td>
	                        <td nowrap> <a href="edit_bulk_import.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td nowrap> 3 </td>
	                        <td> 28200 </td>
	                        <td nowrap> HELUKABEL PVT LTD </td>
	                        <td nowrap> <a href="edit_bulk_import.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td nowrap> 4 </td>
	                        <td nowrap> 13219 </td>
	                        <td nowrap> FUSION ENGINEERING </td>
	                        <td> <a href="edit_bulk_import.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                    <tr>
	                        <td nowrap> 5 </td>
	                        <td nowrap> 13133 </td>
	                        <td nowrap> SHIVAM COMPANY </td>
	                        <td nowrap> <a href="edit_bulk_import.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a> <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
	                    </tr>
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
	$('#tblvendor').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();

     $(document).ready(function() {
    $('.select2-hidden-accessible').select2();
    });
</script>