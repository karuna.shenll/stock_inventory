<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="../assets/layouts/layout/img/de-active/disposal.png" class="imgbasline"> Items Disposal/Destruction</div>
            <div class="actions">
                <!-- <a href="add_sub_subcategory.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Sub Sub-Category</a> -->
            </div>
        </div>
        <div class="portlet-body">
	        <form name="frm_data" id="frm_data" action="store_return.php" class="form-horizontal" method="POST">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Supplier Name <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select  class="form-control" name="supplier_name" id="supplier_name">
                                <option value="">Select Supplier</option>
                                <option value="1">Abb ltd</option>
                                <option value="2">Ritestar</option>
                                <option value="3">Helukabel pvt ltd</option>
                                <option value="4">Fusion engineering</option>
                                <option value="5">Shivam company</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Stock Issuance Id <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select  class="form-control" name="supplier_issuance" id="supplier_issuance">
                                <option value="">Select Stock Issuance</option>
                                <option value="1">SIT1001</option>
                            </select>
                        </div>
                    </div>
                    <div class="storereturndisp" style="display:none">
                        <div class="row">
                            <div class="col-md-12">
                                <table class="table table-striped table-bordered table-hover" id="tblreturnstore">
                                    <thead>
                                        <tr>
                                            <th>Serial No</th>
                                            <th>Item Name</th>
                                            <th>Quantity</th>
                                            <th>Amount</th>
                                            <th>Disposal/Destruction Qty</th>
                                            <th>
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox" class="checkboxes" name="export_all" onclick="checkAllExp(this)">
                                                    <span></span>
                                                </label>
                                           </th>
                                            <!--<th>Action</th> -->
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>9001</td>
                                            <td>Book</td>
                                            <td>10</td>
                                            <td>25</td>
                                            <td><input type="text" class="form-control" name="return_qty" id="return_qty" placeholder="Return Quantity" value="5"></td>
                                            <td>
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox" name="checkbox[]" id="checkbox" value="1" class="checkboxes" onclick="eachChk()">
                                                    <span></span>
                                                </label>  
                                            </td>
                                           <!--  <td><textarea class="form-control" name="remark" id="remark" placeholder="Remark">Damaged</textarea></td>
                                            <td><button type="submit" class="btn green customsavebtn"><i class="fa fa-check"></i> Return </button></td> -->
                                        </tr>
                                        <tr>
                                            <td>9002</td>
                                            <td>Pen</td>
                                            <td>5</td>
                                            <td>25</td>
                                            <td><input type="text" class="form-control" name="return_qty" id="return_qty" placeholder="Return Quantity" value="0"></td>
                                            <td>
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox" name="checkbox[]" id="checkbox" value="1" class="checkboxes" onclick="eachChk()">
                                                    <span></span>
                                                </label>  
                                            </td>
                                            <!-- <td><textarea class="form-control" name="remark" id="remark" placeholder="Remark">Damaged</textarea></td>
                                            <td><button type="submit" class="btn green customsavebtn"><i class="fa fa-check"></i> Return </button></td> -->
                                        </tr>
                                        <tr>
                                            <td>9003</td>
                                            <td>Box</td>
                                            <td>5</td>
                                            <td>25</td>
                                            <td><input type="text" class="form-control" name="return_qty" id="return_qty" placeholder="Return Quantity" value="2"></td>
                                            <td>
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox" name="checkbox[]" id="checkbox" value="1" class="checkboxes" onclick="eachChk()">
                                                    <span></span>
                                                </label>  
                                            </td>
                                           <!--  <td><textarea class="form-control" name="remark" id="remark" placeholder="Remark">Damaged</textarea></td>
                                            <td><button type="submit" class="btn green customsavebtn"><i class="fa fa-check"></i> Return </button></td> -->
                                        </tr>
                                        <tr>
                                            <td>9004</td>
                                            <td>Paper</td>
                                            <td>25</td>
                                            <td>25</td>
                                           <td><input type="text" class="form-control" name="return_qty" id="return_qty" placeholder="Return Quantity" value="10"></td>
                                           <td>
                                                <label class="mt-checkbox mt-checkbox-outline">
                                                    <input type="checkbox" name="checkbox[]" id="checkbox" value="1" class="checkboxes" onclick="eachChk()">
                                                    <span></span>
                                                </label>  
                                            </td>
                                            <!-- <td><textarea class="form-control" name="remark" id="remark" placeholder="Remark">Damaged</textarea></td>
                                            <td><button type="submit" class="btn green customsavebtn"><i class="fa fa-check"></i> Return </button></td> -->
                                        </tr>
                                    </tbody>
                                </table>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-12 text-right">
                                            <button type="submit" class="btn green customsavebtn adminapprovel">
                                                <i class="fa fa-check"></i> Save
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>
<script>
    $(document).on("change","#supplier_issuance",function(){
       var id= $(this).val();
        if(id=="1"){
            $(".storereturndisp").css("display","block");
        } else {
            $(".storereturndisp").css("display","none");
        }
    });
    function checkAllExp(Element)
    {
        chk_len=$('input[id=checkbox]').length;
        if(Element.checked){
            if(chk_len>0){
                $('input[id=checkbox]').prop('checked', true);
            }
        }
        else{
            if(chk_len>0){
                $('input[id=checkbox]').prop('checked', false);
            }
        }
    }
    function eachChk()
    {
        chk_len=$('input[id=checkbox]').length;
        chk_len1=$('input[id=checkbox]:checked').length;
        if(chk_len==chk_len1){
            $('input[name=export_all]').prop('checked', true);
        }
        else{
            $('input[name=export_all]').prop('checked', false);
        }
    }
    var status;
$(document).ready(function() {
    $(".adminapprovel").click(function(){
        chk_len = $('input[id=checkbox]:checked').length;
        if(chk_len==0)
        {
            alert("Please select atleast one Items Disposal/Destruction ?");
            return false;
        } else {
            return false;
        }
    });
});

</script>