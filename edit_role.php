<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
if (isset($_GET["id"])) {
    $hnd_id=$_GET["id"];
    if ($_GET["id"]==1) {
        $role_name ="Admin";
        $role_description = "Accessing Admin Panel";
    }
    if ($_GET["id"]==2) {
        $role_name ="User";
        $role_description = "Accessing User Panel";
    }
     if ($_GET["id"]==3) {
        $role_name ="Inspector";
        $role_description = "Inspector";
    }
     if ($_GET["id"]==4) {
        $role_name ="Report Viewer";
        $role_description = "Report";
    }
     if ($_GET["id"]==5) {
        $role_name ="Supervisor";
        $role_description = "Supervisor";
    }
}
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/setting.png" class="imgbasline"> Edit Role</div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_role" id="frm_role" action="role_list.php?msg=2" class="form-horizontal" method="POST">
                <input type="hidden" class="form-control" name="hnd_id" id="hnd_id"  value="<?php echo $hnd_id?>">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Role Name <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="role_name" id="role_name" placeholder="Role Name" value="<?php echo $role_name?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Role Description <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="role_description" id="role_description" placeholder="Role Description" value="<?php echo $role_description?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Admin Role </label>

                         <div class="col-md-3">
                            <label class="control-label">Manage Stores</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_view" class="store"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_add" class="store">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_edit" class="store"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Manage Outlet</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_view"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_add">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_edit"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Manage Suppliers</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="supplier_view" class="supplier"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="supplier_add" class="supplier">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="supplier_edit" class="supplier"> Edit / Delete
                                    <span></span>
                                </label>
                            </div>
                        </div>

                         
                      <!--   <div class="col-md-3">
                            <label class="control-label">Manage Categories</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="category_view" class="category"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="category_add" class="category">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="category_edit" class="category"> Edit / Delete 
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Manage Sub Categories</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="subcategory_view" class="subcategory"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="subcategory_add" class="subcategory">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="subcategory_edit" class="subcategory"> Edit / Delete 
                                    <span></span>
                                </label>
                            </div>
                        </div> -->
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                       <!--  <div class="col-md-3">
                            <label class="control-label">Manage Sub Sub-Categories</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="sub_category_view" class="subsubcategory"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="sub_category_add" class="subsubcategory">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="sub_category_edit" class="subsubcategory"> Edit / Delete 
                                    <span></span>
                                </label>
                            </div>
                        </div> -->
                        <div class="col-md-3">
                            <label class="control-label">Manage Items</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="item_view" class="item"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="item_add" class="item">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="item_edit" class="item"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Manage Users</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="user_view" class="users"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                     <input type="checkbox" name="codes[]" value="user_add" class="users">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="user_edit" class="users"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>

                        <div class="col-md-3">
                            <label class="control-label">Manage Roles</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_view" class="role"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_add" class="role">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_edit" class="role"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        
                         

                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Customer Role </label> <div class="col-md-3">
                             <label class="control-label">Stock Receipts</label>
                             <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_view" class="bulkimport"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_add" class="bulkimport">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_edit" class="bulkimport"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>
                         <div class="col-md-3">
                             <label class="control-label">Stock Transfer</label>
                             <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_view" class="datainput"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_add" class="datainput">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_edit" class="datainput"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                             <label class="control-label">Stock Issuance</label>
                             <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_view" class="datainput"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_add" class="datainput">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_edit" class="datainput"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        

                        <!-- <div class="col-md-3">
                            <div class="mt-checkbox-list cusrolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="bulk_view" class="bulkimport"> Bulk Import
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mt-checkbox-list cusrolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="store_return" class="datainput"> Store Return
                                    <span></span>
                                </label>
                            </div>
                        </div> -->
                       <!--  <div class="col-md-3">
                            <div class="mt-checkbox-list cusrolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="items_disposal" class="datainput"> Items Disposal/Destruction
                                    <span></span>
                                </label>
                            </div>
                        </div> -->
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <!-- <div class="col-md-3">
                            <label class="control-label">Stock Issuance / Transfers List</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="stock_issuance" class="datainput"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="stock_issuance" class="datainput">  Add Pge
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="stock_issuance" class="datainput"> Edit Page
                                    <span></span>
                                </label>
                            </div>
                        </div> -->
                         <div class="col-md-3">
                             <label class="control-label">Stock Return</label>
                             <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_view" class="datainput"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_add" class="datainput">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_edit" class="datainput"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Reports</label>
                            <div class="mt-checkbox-list rolecheck">
                                 <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_received" class="reportsview"> Total Stock Receipts
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_issued" class="reportsview">  Total Stock Transfer
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_issued" class="reportsview">  Total Stock Issuance
                                    <span></span>
                                </label>
                                 <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_returns" class="reportsview"> Total Stock Returns
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_stock" class="reportsview"> Total in Stock Available
                                    <span></span>
                                </label>
                               <!--  <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_received" class="reportsview"> Total Received
                                    <span></span>
                                </label> -->
                               <!--  <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_issued" class="reportsview">  Total Stock Transactions
                                    <span></span>
                                </label>
                                 <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_returns" class="reportsview"> Total Stock Returns
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_stock" class="reportsview"> Total in Stock Available
                                    <span></span>
                                </label> -->
                               
                               <!--  <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_disposals" class="reportsview"> Total Destructions/Disposals
                                    <span></span>
                                </label> -->
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mt-checkbox-list cusrolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="datainput_view" class="datainput"> Stock Search
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn green customsavebtn">
                                <i class="fa fa-check"></i> Save
                            </button>
                            <a href="role_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>
<script>
    $(document).ready(function(){
        var roleid= $("#hnd_id").val();
        if (roleid=="1") {
            $('.supplier,.category,.subcategory,.subsubcategory,.item,.users,.role,.store').prop('checked', true);
        } else if (roleid=="2") {
            $('.supplier,.category,.subcategory,.subsubcategory,.item,.store').prop('checked', true);
        } else if (roleid=="3") {
            $('.datainput,.bulkimport').prop('checked', true);
        } else if (roleid=="4") {
            $('.reportsview').prop('checked', true);
        } else if (roleid=="5") {
            $('.datainput,.bulkimport,.reportsview').prop('checked', true);
        } else{
             $('.supplier,.category,.subcategory,.subsubcategory,.item,.users,.role,.datainput,.bulkimport,.reportsview').prop('checked', false);
        }
    });
</script>