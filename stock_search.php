<?php 
session_start();
include("session_check.php"); 
include("header.php"); 

?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
	
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/area.png" class="imgbasline"> Stock Search List </div>
            <div class="actions">
            	<a href="export_stock_search.xls" class="btn green btn-sm excelbtn"><i class="fa fa-download"></i> Export to Excel</a>
               <!--  <a href="add_item.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Item </a> -->
            </div>
        </div>
        <div class="portlet-body">
	        <div class="row">
	        	<div class="col-md-12 paddingleftright">
					<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<select class="form-control select2" name="supplier_name" id="supplier_name">
                                <option value="">Select Store</option>
                                <option value="1">Fine Line </option>
                                <option value="2">Lulu Salmiya</option>
                                <option value="3">Defence Co Op</option>
                                <option value="4">Classic</option>
                                <option value="4">Al Meera</option>
                            </select>
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                            <select class="form-control select2" name="supplier_name" id="supplier_name">
                                <option value="">Select Manufacturer</option>
                                <option value="1">Arvato</option>
                                <option value="2">RHEIM</option>
                                <option value="3">Defence Co Op</option>
                                <option value="4">CCS DIGITAL</option>
                            </select>
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="part_number" id="part_number" placeholder="Part Number">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="part_des" id="part_des" placeholder="Part Description">
                        </div>
	        		</div>
	        		<div class="col-md-3 paddingbottom">
	        			<div class="col-md-12 paddingleftright">
                        	<input type="text" class="form-control" name="apple_id" id="apple_id" placeholder="Apple ID">
                        </div>
	        		</div>
	        		
	        		<div class="col-md-3">
	        			<div class="col-md-12 paddingleftright">
	        				<button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
	        				<a href="stock_search.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
	        			</div>
	        		</div>
	        	</div>
	        </div>
        	<div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
	            <table class="table table-striped table-bordered table-hover" id="tblrole">
	            	<thead>
	                    <tr>
	                        <th> SI.NO </th>
	                        <th> Part Number </th>
	                        <th> Description </th>
	                        <th> Apple ID </th>
	                        <th> Qty </th>
	                        <th> Remarks </th>
	                    </tr>
	                </thead>
	                </tbody>
	                    <tr>
	                        <td> 1 </td>
	                        <td> 818-02365</td>
	                        <td> IOS Dock Adhesive removal tool</td>
	                        <td> 546881 </td>
	                        <td> 4 </td>
	                        <td> QATAR APR SPARES</td>
	                    </tr>
	                    <tr>
	                        <td> 2 </td>
	                        <td> 622-00119 </td>
	                         <td>Security Cable : Smart Keyboard </td>
	                        <td> 1093880 </td>
	                        <td> 3 </td>
	                        <td>IQ VILLAGIO MALL</td>
	                    </tr>
	                    <tr>
	                        <td> 3 </td>
	                        <td> 677-03900 </td>
	                        <td> Apple TV Base Unit</td>
	                        <td> 1599662 </td>
	                        <td> 5 </td>
	                        <td>QATAR MOBILITY SPARES</td>
	                    </tr>
	                     <tr>
	                        <td> 4 </td>
	                        <td> 677-03901 </td>
	                        <td> Apple TV Base Kit</td>
	                        <td> 1599661 </td>
	                        <td> 7 </td>
	                        <td>QATAR APPLE SHOP 2.0 SPARES</td>
	                    </tr>

	                    <tr>
	                        <td> 5 </td>
	                        <td> SK-989P-W</td>
	                        <td> Mac mini Sekure Little Button Sensor</td>
	                        <td> 546881 </td>
	                        <td> 4 </td>
	                        <td> QATAR APR SPARES</td>
	                    </tr>
	                    <tr>
	                        <td> 6 </td>
	                        <td> 622-00059 </td>
	                         <td>Sekure 12 Position Alarm Box Alkaline Ba </td>
	                        <td> 1093880 </td>
	                        <td> 3 </td>
	                        <td>IQ VILLAGIO MALL</td>
	                    </tr>
	                    <tr>
	                        <td> 7 </td>
	                        <td> 013-0162-0024 </td>
	                        <td> MTI 3.0 Carbon Tether - Standard</td>
	                        <td> 1093880 </td>
	                        <td> 5 </td>
	                        <td>QATAR MOBILITY SPARES</td>
	                    </tr>
	                     <tr>
	                        <td> 8 </td>
	                        <td> HA54-46-45 </td>
	                        <td> 2.5mm Allen Wrench Key 1/EA</td>
	                        <td> 1599661 </td>
	                        <td> 7 </td>
	                        <td>QATAR APPLE SHOP 2.0 SPARES</td>
	                    </tr>

	                    <tr>
	                        <td> 9 </td>
	                        <td> 3C556Z/A</td>
	                        <td> Watch face + White Bracelet 42mm silver</td>
	                        <td> 546881 </td>
	                        <td> 4 </td>
	                        <td> QATAR APR SPARES</td>
	                    </tr>
	                    <tr>
	                        <td> 10 </td>
	                        <td> 622-00094 </td>
	                         <td>PRO Security Cable 1EA </td>
	                        <td> 1093880 </td>
	                        <td> 3 </td>
	                        <td>IQ VILLAGIO MALL</td>
	                    </tr>
	                    <tr>
	                        <td> 11 </td>
	                        <td> 818-02816</td>
	                        <td> iPad Pro 12.9" Puck Locator</td>
	                        <td> 1599662 </td>
	                        <td> 5 </td>
	                        <td>QATAR MOBILITY SPARES</td>
	                    </tr>
	                     <tr>
	                        <td> 12 </td>
	                        <td> 622-0524 </td>
	                        <td> IPD combo cable 1/EA UTE</td>
	                        <td> 1599661 </td>
	                        <td> 10 </td>
	                        <td>QATAR APPLE SHOP 2.0 SPARES</td>
	                    </tr>
	                     <tr>
	                        <td> 13 </td>
	                        <td> 622-00119 </td>
	                        <td> Security Cable : Smart Keyboard</td>
	                        <td> 1599661 </td>
	                        <td> 10 </td>
	                        <td>QATAR APPLE SHOP 2.0 SPARES</td>
	                    </tr>
	                     <tr>
	                        <td> 14 </td>
	                        <td> 622-00072 </td>
	                        <td> MB USB-C Combo Cable 1/EA</td>
	                        <td> 1599661 </td>
	                        <td> 7 </td>
	                        <td>QATAR APPLE SHOP 2.0 SPARES</td>
	                    </tr>
	                     <tr>
	                        <td> 15 </td>
	                        <td> 165-00076</td>
	                        <td> acxv:touch & try security cables 38mm</td>
	                        <td> 1599661 </td>
	                        <td> 8 </td>
	                        <td>QATAR APPLE SHOP 2.0 SPARES</td>
	                    </tr>
	                   
	                </tbody>
	            </table>
	        </div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
	$('#tblrole').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":20 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();

    $(document).ready(function() {
    $('.select2-hidden-accessible').select2();
    });
</script>