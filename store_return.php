<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/return.png" class="imgbasline"> Add Stock Return</div>
            <div class="actions">
                <!-- <a href="add_sub_subcategory.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Sub Sub-Category</a> -->
            </div>
        </div>
        <div class="portlet-body">
	        <form name="frm_data" id="frm_data" action="storereturn_list.php" class="form-horizontal" method="POST">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Outlet<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control select2" name="outlet_name" id="outlet_name">
                                <option value="">Select Outlet</option>
                                <option value="1" selected>Axiom Delma Mall </option>
                                <option value="2">Al Thalla Cafeteria</option>
                                <option value="3">Ashjan Grocery</option>
                                <option value="4">Shafi Grocery</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Store<span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select  class="form-control select2" name="supplier_code" id="supplier_code">
                                <option value="">Select Store</option>
                                <option value="1" selected="">Al Meera</option>
                                <option value="2">Lulu Salmiya</option>
                                <option value="3">Defence Co Op</option>
                                <option value="4">Carrefour</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Return Date <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="return_date" id="return_date" placeholder="Return Date" autocomplete="off" data-date-format="dd/mm/yyyy" value="28/01/2019">
                        </div>
                    </div>
                    <div class="storereturndisp" style="display:none">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="table-responsive">
                                    <table class="table table-striped table-bordered table-hover" id="tblstorerecipt">
                                        <thead>
                                            <tr>
                                                <!-- <th nowrap>Item</th> -->
                                                <th nowrap>Part Number</th>
                                                <th nowrap>Description</th>
                                                <th nowrap class="text-center">Apple ID</th>
                                                <th nowrap>Quantity</th>
                                                <th nowrap>Date</th>
                                                <th nowrap>Condition</th>
                                                <th nowrap>Remarks</th>
                                                <th nowrap class="text-center">Image</th>
                                                <th nowrap>Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                               <!--  <td nowrap>Apple TV Base Unit</td> -->
                                                <td nowrap>  <input type="text" class="form-control" name="part_number[]" id="part_number" placeholder="Part Number" value="622-00084"></td>
                                                <td nowrap>Apple TV Base Unit</td>
                                                <td nowrap class="text-center">1093880</td>
                                                <td><input type="text" class="form-control" name="quantity[]" id="quantity" placeholder="Return Quantity" value="2" style="width: 100px;"></td>
                                                <td> <input type="text" class="form-control stock_date" name="stock_date[]" id="stock_date" placeholder="Date" value="29/01/2019" style="width: 100px;"></td>
                                                <td> <input type="text" class="form-control" name="condition[]" id="condition" placeholder="Condition" value="Normal"></td>
                                                <td>
                                                    <textarea class="form-control" name="remark[]" id="remark" placeholder="Remarks">QATAR APR SPARES</textarea>
                                                </td>
                                                <td nowrap class="text-center">
                                                    <div class="image-upload">
                                                        <label for="imgupload">
                                                            <img src="assets/pages/img/upload-icon.png"/>
                                                        </label>
                                                        <input type="file" class="form-control" name="imgupload[]" id="imgupload"/>
                                                    </div>
                                                    <a href="javascript:void(0);" class="imagepopup"> <span>3</span></a>
                                                </td>
                                                <td>
                                                    <select class="form-control" name="status[]" id="status">
                                                        <option value="">Select</option>
                                                        <option value="Damaged" selected>Damaged</option>
                                                        <option value="Broken">Broken</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap>  <input type="text" class="form-control" name="part_number[]" id="part_number" placeholder="Part Number" value="677-01423"></td>
                                                <td nowrap>Apple TV Cable Clip</td>
                                                <td nowrap class="text-center">1599662</td>
                                                <td><input type="text" class="form-control" name="quantity[]" id="quantity" placeholder="Return Quantity" value="5" style="width: 100px;"></td>
                                                <td> <input type="text" class="form-control stock_date" name="stock_date[]" id="stock_date" placeholder="Date" value="30/01/2019" style="width: 100px;"></td>
                                                <td> <input type="text" class="form-control" name="condition[]" id="condition" placeholder="Condition" value="Normal"></td>
                                                <td>
                                                    <textarea class="form-control" name="remark[]" id="remark" placeholder="Remarks">virgin megastore villagio</textarea>
                                                </td>
                                                <td nowrap class="text-center">
                                                    <div class="image-upload">
                                                        <label for="imgupload">
                                                            <img src="assets/pages/img/upload-icon.png"/>
                                                        </label>
                                                        <input type="file" class="form-control" name="imgupload[]" id="imgupload"/>
                                                    </div>
                                                    <a href="javascript:void(0);" class="imagepopup"><span>2</span></a>
                                                </td>
                                                <td>
                                                    <select class="form-control" name="status[]" id="status">
                                                        <option value="">Select</option>
                                                        <option value="Damaged" >Damaged</option>
                                                        <option value="Broken" selected>Broken</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <!-- <td nowrap >USBC-USBA Cable</td> -->
                                                <td nowrap>  <input type="text" class="form-control" name="part_number[]" id="part_number" placeholder="Part Number" value="622-00072"></td>
                                                <td nowrap>USBC-USBA Cable</td>
                                                <td nowrap class="text-center">XXXXQATAPR</td>
                                                <td><input type="text" class="form-control" name="quantity[]" id="quantity" placeholder="Return Quantity" value="4" style="width: 100px;"></td>
                                                <td> <input type="text" class="form-control stock_date" name="stock_date[]" id="stock_date" placeholder="Date" value="31/01/2019" style="width: 100px;"></td>
                                                <td> <input type="text" class="form-control" name="condition[]" id="condition" placeholder="Condition" value="Normal"></td>
                                                <td>
                                                    <textarea class="form-control" name="remark[]" id="remark" placeholder="Remarks">QATAR APR SPARES</textarea>
                                                </td>
                                                <td nowrap class="text-center">
                                                    <div class="image-upload">
                                                        <label for="imgupload">
                                                            <img src="assets/pages/img/upload-icon.png"/>
                                                        </label>
                                                        <input type="file" class="form-control" name="imgupload[]" id="imgupload"/>
                                                    </div>
                                                    <a href="javascript:void(0);" class="imagepopup"> <span>1</span></a>
                                                </td>
                                                <td>
                                                    <select class="form-control" name="status[]" id="status">
                                                        <option value="">Select</option>
                                                        <option value="Damaged" selected>Damaged</option>
                                                        <option value="Broken" >Broken</option>
                                                    </select>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td nowrap>  <input type="text" class="form-control" name="part_number[]" id="part_number" placeholder="Part Number" value="622-00119"></td>
                                                <td nowrap>SR IPAD PRO WARM GPF24</td>
                                                <td nowrap class="text-center">XXXXQATWHITE</td>
                                                <td><input type="text" class="form-control" name="quantity[]" id="quantity" placeholder="Return Quantity" value="6" style="width: 100px;"></td>
                                                <td> <input type="text" class="form-control stock_date" name="stock_date[]" id="stock_date" placeholder="Date" value="01/02/2019" style="width: 100px;"></td>
                                                <td> <input type="text" class="form-control" name="condition[]" id="condition" placeholder="Condition" value="Normal"></td>
                                                <td>
                                                    <textarea class="form-control" name="remark[]" id="remark" placeholder="Remarks">virgin megastore villagio</textarea>
                                                </td>
                                                <td nowrap class="text-center">
                                                    <div class="image-upload">
                                                        <label for="imgupload">
                                                            <img src="assets/pages/img/upload-icon.png"/>
                                                        </label>
                                                        <input type="file" class="form-control" name="imgupload[]" id="imgupload"/>
                                                    </div>
                                                    <a href="javascript:void(0);" class="imagepopup"> <span>4</span></a>
                                                </td>
                                                <td>
                                                    <select class="form-control" name="status[]" id="status">
                                                        <option value="">Select</option>
                                                        <option value="Damaged" >Damaged</option>
                                                        <option value="Broken" selected>Broken</option>
                                                    </select>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-12 text-right">
                                    <button type="submit" class="btn green customsavebtn">
                                        <i class="fa fa-check"></i> Save
                                    </button>
                                    <a href="storereturn_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Cancel</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<div class="modal fade in" id="imagePopup" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><b>View Image</b></h4>
            </div>
            <div class="modal-body"> 
                <div class="row">
                    <div class="col-md-11" style="margin-top: 10px;margin-bottom: 15px;">
                        <div class="form-group">
                            <div class="col-md-12">
                                <div class="col-md-4">
                                    <img src="assets/pages/img/iphone.jpg" class="img-responsive" style="border:1px solid #ddd;">
                                </div>
                                <div class="col-md-4">
                                    <img src="assets/pages/img/phone.png" class="img-responsive" style="border:1px solid #ddd;">
                                </div>
                                <div class="col-md-4">
                                   <img src="assets/pages/img/xperia.png" class="img-responsive" style="border:1px solid #ddd;">
                                </div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn red customrestbtn"  data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>
<script>
     $(document).ready(function() {
    var transfers_code = $('#supplier_code option:selected').text();
    if (transfers_code!="") {
         $(".storereturndisp").css("display","block");
    } else {
        $(".storereturndisp").css("display","none");
    }
    $(document).on("change","#supplier_code",function(){
       var id= $(this).val();
        if(id!=""){
            $(".storereturndisp").css("display","block");
        } else {
            $(".storereturndisp").css("display","none");
        }
    });
});

     $(document).ready(function() {
    $('.select2-hidden-accessible').select2();
    });
     $(function() {
    $("#return_date,.stock_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});
$(document).ready(function () {
        $(".imagepopup").click(function(){
             $('#imagePopup').modal('show');
        });
    });
     
</script>