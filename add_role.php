<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/setting.png" class="imgbasline"> Add Role</div>
            <div class="tools">
            </div>
        </div>
        <div class="portlet-body form">
            <!-- BEGIN FORM-->
            <form name="frm_role" id="frm_role" action="role_list.php?msg=1" class="form-horizontal" method="POST">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Role Name <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="role_name" id="role_name" placeholder="Role Name" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Role Description <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="role_description" id="role_description" placeholder="Role Description" value="">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label">Admin Role </label>

                        <div class="col-md-3">
                            <label class="control-label">Manage Stores</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_view"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_add">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_edit"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Manage Outlet</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_view"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_add">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_edit"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Manage Suppliers</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="supplier_view"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="supplier_add">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="supplier_edit"> Edit / Delete 
                                    <span></span>
                                </label>
                            </div>
                        </div>

                         

                       
                        <!-- <div class="col-md-3">
                            <label class="control-label">Manage Categories</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="category_view"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="category_add">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="category_edit"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Manage Sub Categories</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="subcategory_view"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="subcategory_add">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="subcategory_edit"> Edit / Delete 
                                    <span></span>
                                </label>
                            </div>
                        </div> -->
                    </div>
                    <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                       <!--  <div class="col-md-3">
                            <label class="control-label">Manage Sub Sub-Categories</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="sub_category_view"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="sub_category_add">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="sub_category_edit"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div> -->
                          <div class="col-md-3">
                            <label class="control-label">Manage Items</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="item_view"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="item_add">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="item_edit"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>
                          <div class="col-md-3">
                            <label class="control-label">Manage Users</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="user_view"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                     <input type="checkbox" name="codes[]" value="user_add">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="user_edit"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>

                         <div class="col-md-3">
                            <label class="control-label">Manage Roles</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_view"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_add">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_edit"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>
                       
                       
                    </div>
                    
                    <!-- <div class="form-group">
                        <label class="col-md-3 control-label">Customer Role</label>
                        <div class="col-md-3">
                            <div class="mt-checkbox-list cusrolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="datainput_view" class="datainput"> Data Input Form
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mt-checkbox-list cusrolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="bulk_view" class="bulkimport"> Bulk Import
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <label class="control-label">Reports</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_received" class="reportsview"> Total Received
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_issued" class="reportsview">  Total Issued
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_stock" class="reportsview"> Total in Stock Available
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_returns" class="reportsview"> Total Returns
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_disposals" class="reportsview"> Total Destructions/Disposals
                                    <span></span>
                                </label>
                            </div>
                        </div>
                    </div> -->

                     <div class="form-group">
                        <label class="col-md-3 control-label">User Role</label>
                        <div class="col-md-3">
                             <label class="control-label">Stock Receipts</label>
                             <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_view"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_add">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_edit"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>

                         <div class="col-md-3">
                             <label class="control-label">Stock Transfer</label>
                             <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_view"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_add">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_edit"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                             <label class="control-label">Stock Issuance</label>
                             <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_view"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_add">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_edit"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>
                         
                </div>
                 <div class="form-group">
                        <label class="col-md-3 control-label"></label>
                        <div class="col-md-3">
                             <label class="control-label">Stock Return</label>
                             <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_view"> View 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_add">  Add 
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="role_edit"> Edit / Delete  
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-3">
                             <label class="control-label">Reports</label>
                            <div class="mt-checkbox-list rolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_received" class="reportsview"> Total Stock Receipts
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_issued" class="reportsview">  Total Stock Transfer
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_issued" class="reportsview">  Total Stock Issuance
                                    <span></span>
                                </label>
                                 <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_returns" class="reportsview"> Total Stock Returns
                                    <span></span>
                                </label>
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_stock" class="reportsview"> Total in Stock Available
                                    <span></span>
                                </label>
                               
                               <!--  <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="report_disposals" class="reportsview"> Total Destructions/Disposals
                                    <span></span>
                                </label> -->
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="mt-checkbox-list cusrolecheck">
                                <label class="mt-checkbox">
                                    <input type="checkbox" name="codes[]" value="datainput_view" class="datainput"> Stock Search
                                    <span></span>
                                </label>
                            </div>
                        </div>
                        
                    </div>

                        
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn green customsavebtn">
                                <i class="fa fa-check"></i> Save
                            </button>
                            <a href="role_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
            <!-- END FORM-->
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>