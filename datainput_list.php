<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/edit-form.png" class="imgbasline"> Data Input Form</div>
            <div class="actions">
                <!-- <a href="add_sub_subcategory.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Sub Sub-Category</a> -->
            </div>
        </div>
        <div class="portlet-body">
	        <form name="frm_data" id="frm_data" action="datainput_list.php" class="form-horizontal" method="POST">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Supplier Name <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select  class="form-control" name="supplier_name" id="supplier_name">
                                <option value="">Select Supplier</option>
                                <option value="1">Abb ltd</option>
                                <option value="2">Ritestar</option>
                                <option value="3">Helukabel pvt ltd</option>
                                <option value="4">Fusion engineering</option>
                                <option value="5">Shivam company</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Data Upload <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="file" class="form-control" name="bulk_upload" id="bulk_upload">
                            <span class="downloadexcel"><a style="display: inline-block;text-decoration: none;margin-top:5px;" href="download.xls"><img src="../assets/pages/img/download.png"> Click Here To Download Data Upload Excel Format</a></span>
                        </div>
                    </div>
                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn green customsavebtn">
                                <i class="fa fa-check"></i> Save
                            </button>
                            <a href="datainput_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Cancel</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
	$('#tblrole').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();
</script>