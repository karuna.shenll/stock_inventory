<?php 
session_start();
include("session_check.php"); 
include("header.php");


$item=$_REQUEST['item_status'];
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/reports.png" class="imgbasline"> Advanced Stock Report</div>
            <div class="actions">
                <!-- <a href="add_sub_subcategory.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Sub Sub-Category</a> -->
            </div>
        </div>
        <div class="portlet-body">

             <div class="row">
                <form action="stock_report.php" id="frm_stockreport" name="frm_stockreport" method="post">
                <div class="col-md-12 paddingleftright">
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                              <input type="text" class="form-control" name="from_date" id="from_date" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="From Date" value="<?php //echo date('d-m-Y'); ?>" >
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="to_date" id="to_date" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="To Date" value="<?php //echo date('d-m-Y'); ?>" >
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <select class="form-control select2" name="supplier_id" id="supplier_id">
                                <option value="">Select Supplier</option>
                                <option value="Bahrain">Supplier 1</option>
                                <option value="Saudi Arabia">Supplier 2</option>
                                <option value="Qatar">Supplier 3</option>
                                <option value="UAE">Supplier 4</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                           <select class="form-control select2" name="category_id" id="category_id" >
                                <option value="">Select Category</option>
                                <option value="Electronics">Electronics</option>
                                <option value="Home Appliance">Home Appliance</option>
                                <option value="Food Products">Food Products</option>
                                <option value="Stationary products">Stationary products</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <select class="form-control select2" name="item_status" id="item_status">
                                <option value="">Item Status</option>
                                <option value="1" <?php if($item==1) { echo 'selected'; } ?>>Total Received </option>
                                <option value="2" <?php if($item==2) { echo 'selected'; } ?>>Total Issued </option>
                                <option value="3" <?php if($item==3) { echo 'selected'; } ?>>Total in Stock Available </option>
                                <option value="4" <?php if($item==4) { echo 'selected'; } ?>>Total Return </option>
                                <option value="5" <?php if($item==5) { echo 'selected'; } ?>>Total Destructions/Disposals </option>
                            </select>
                        </div>
                    </div>     
                    <div class="col-md-3">
                        <div class="col-md-12 paddingleftright">
                            <button type="submit" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
                            <a href="supplier_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
                        </div>
                    </div>
                </div>
            </form>
            </div>
            <div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
                <?php if($item==1) { ?>
                <table class="table table-striped table-bordered table-hover" id="tblrole">
                    <thead>
                        <tr>
                            <th nowrap> SI.NO </th>
                            <th nowrap> Serial No </th>
                            <th nowrap> Supplier Name </th>
                            <th nowrap> Category Name </th>
                            <th nowrap> Item Name </th>
                            <th nowrap> Received Quantity </th>
                            <th nowrap> Received Date </th>
                        </tr>
                    </thead>
                    </tbody>
                        <tr>
                            <td> 1 </td>
                            <td> ST0001 </td>
                            <td> ABB LTD </td>
                            <td> Electronics</td>
                            <td> Mobile Panel</td>
                            <td> 10</td>
                            <td> 23/01/2019</td>
                        </tr>
                        <tr>
                            <td> 2 </td>
                            <td> ST0002 </td>
                            <td> RITESTAR </td>
                            <td> Home Appliance </td>
                            <td> Chairs</td>
                            <td> 30</td>
                            <td> 20/01/2019</td>
                        </tr>
                        <tr>
                            <td> 3 </td>
                            <td> ST0003 </td>
                            <td> HELUKABEL PVT LTD </td>
                            <td> Food Products </td>
                            <td> Chicken</td>
                            <td> 5 </td>
                            <td> 19/01/2019</td>
                        </tr>
                        <tr>
                            <td> 4 </td>
                            <td> ST0004 </td>
                            <td> FUSION</td>
                            <td> Stationary products  </td>
                            <td> Book</td>
                            <td> 50</td>
                            <td> 19/01/2019</td>
                        </tr>
                        <tr>
                            <td> 5 </td>
                            <td> ST0005 </td>
                            <td> SHIVAM PVT LTD   </td>
                            <td> Cloths</td>
                            <td> T-shirt</td>
                            <td> 20</td>
                            <td> 18/01/2019</td>
                        </tr>
                    </tbody>
                </table>
            <?php } ?>
             <?php if($item==2) { ?>
                <table class="table table-striped table-bordered table-hover" id="tblrole">
                    <thead>
                        <tr>
                            <th nowrap> SI.NO </th>
                            <th nowrap> Serial No </th>
                            <th nowrap> Supplier Name </th>
                            <th nowrap> Category Name </th>
                            <th nowrap> Item Name </th>
                            <th nowrap> Issued Quantity </th>
                            <th nowrap> Issued Date </th>
                        </tr>
                    </thead>
                    </tbody>
                        <tr>
                            <td> 1 </td>
                            <td> ST0001 </td>
                            <td> ABB LTD </td>
                            <td> Electronics</td>
                            <td> Mobile Panel</td>
                            <td> 5 </td>
                            <td> 23/01/2019</td>
                        </tr>
                        <tr>
                            <td> 2 </td>
                            <td> ST0002 </td>
                            <td> RITESTAR </td>
                            <td> Home Appliance </td>
                            <td> Chairs</td>
                            <td> 10 </td>
                            <td> 20/01/2019</td>
                        </tr>
                        <tr>
                            <td> 3 </td>
                            <td> ST0003 </td>
                            <td> HELUKABEL PVT LTD </td>
                            <td> Food Products </td>
                            <td> Ice Cream</td>
                            <td> 5 </td>
                            <td> 19/01/2019</td>
                        </tr>
                        <tr>
                            <td> 4 </td>
                            <td> ST0004 </td>
                            <td> FUSION</td>
                            <td> Stationary products  </td>
                            <td> Book</td>
                            <td> 50</td>
                            <td> 19/01/2019</td>
                        </tr>
                        <tr>
                            <td> 5 </td>
                            <td> ST0005 </td>
                            <td> SHIVAM PVT LTD   </td>
                            <td> Cloths</td>
                            <td> T-shirt</td>
                            <td> 20</td>
                            <td> 18/01/2019</td>
                        </tr>
                    </tbody>
                </table>
            <?php } ?>
             <?php if($item==3) { ?>
                <table class="table table-striped table-bordered table-hover" id="tblrole">
                    <thead>
                        <tr>
                            <th nowrap> SI.NO </th>
                            <th nowrap> Serial No </th>
                           <!--  <th nowrap> Supplier Name </th> -->
                            <th nowrap> Category Name </th>
                            <th nowrap> Item Name </th>
                            <th nowrap> Avaliable Stock Qty </th>
                            <!-- <th nowrap> Destructions / Disposals Date </th> -->
                        </tr>
                    </thead>
                    </tbody>
                        <tr>
                            <td> 1 </td>
                            <td> ST0001 </td>
                            <!-- <td> ABB LTD </td> -->
                            <td> Electronics</td>
                            <td> Mobile Panel</td>
                            <td> 5 </td>
                            <!-- <td> 23/01/2019</td> -->
                        </tr>
                        <tr>
                            <td> 2 </td>
                            <td> ST0002 </td>
                            <!-- <td> RITESTAR </td> -->
                            <td> Home Appliance </td>
                            <td> Chairs</td>
                            <td> 10 </td>
                            <!-- <td> 20/01/2019</td> -->
                        </tr>
                        <tr>
                            <td> 3 </td>
                            <td> ST0003 </td>
                            <!-- <td> HELUKABEL PVT LTD </td> -->
                            <td> Food Products </td>
                            <td> Ice Cream</td>
                            <td> 5 </td>
                           <!--  <td> 19/01/2019</td> -->
                        </tr>
                        <tr>
                            <td> 4 </td>
                            <td> ST0004 </td>
                            <!-- <td> FUSION</td> -->
                            <td> Stationary products  </td>
                            <td> Book</td>
                            <td> 50</td>
                           <!--  <td> 19/01/2019</td> -->
                        </tr>
                        <tr>
                            <td> 5 </td>
                            <td> ST0005 </td>
                            <!-- <td> SHIVAM PVT LTD   </td> -->
                            <td> Cloths</td>
                            <td> T-shirt</td>
                            <td> 20</td>
                            <!-- <td> 18/01/2019</td> -->
                        </tr>
                    </tbody>
                </table>
            <?php } ?>
             <?php if($item==4) { ?>
               <table class="table table-striped table-bordered table-hover" id="tblrole">
                    <thead>
                        <tr>
                            <th nowrap> SI.NO </th>
                            <th nowrap> Serial No </th>
                            <th nowrap> Supplier Name </th>
                            <th nowrap> Category Name </th>
                            <th nowrap> Item Name </th>
                            <th nowrap> Returns Quantity </th>
                            <th nowrap> Returns Date </th>
                        </tr>
                    </thead>
                    </tbody>
                        <tr>
                            <td> 1 </td>
                            <td> ST0001 </td>
                            <td> ABB LTD </td>
                            <td> Electronics</td>
                            <td> Mobile Panel</td>
                            <td> 5 </td>
                            <td> 23/01/2019</td>
                        </tr>
                        <tr>
                            <td> 2 </td>
                            <td> ST0002 </td>
                            <td> RITESTAR </td>
                            <td> Home Appliance </td>
                            <td> Chairs</td>
                            <td> 10 </td>
                            <td> 20/01/2019</td>
                        </tr>
                        <tr>
                            <td> 3 </td>
                            <td> ST0003 </td>
                            <td> HELUKABEL PVT LTD </td>
                            <td> Food Products </td>
                            <td> Ice Cream</td>
                            <td> 0 </td>
                            <td> 19/01/2019</td>
                        </tr>
                        <tr>
                            <td> 4 </td>
                            <td> ST0004 </td>
                            <td> FUSION</td>
                            <td> Stationary products  </td>
                            <td> Book</td>
                            <td> 10</td>
                            <td> 19/01/2019</td>
                        </tr>
                        <tr>
                            <td> 5 </td>
                            <td> ST0005 </td>
                            <td> SHIVAM PVT LTD   </td>
                            <td> Cloths</td>
                            <td> T-shirt</td>
                            <td> 5</td>
                            <td> 18/01/2019</td>
                        </tr>
                    </tbody>
                </table>
            <?php } ?>
             <?php if($item==5) { ?>
                <table class="table table-striped table-bordered table-hover" id="tblrole">
                    <thead>
                        <tr>
                            <th nowrap> SI.NO </th>
                            <th nowrap> Serial No </th>
                            <th nowrap> Supplier Name </th>
                            <th nowrap> Category Name </th>
                            <th nowrap> Item Name </th>
                            <th nowrap> Destructions / Disposals Qty </th>
                            <th nowrap> Destructions / Disposals Date </th>
                        </tr>
                    </thead>
                    </tbody>
                        <tr>
                            <td> 1 </td>
                            <td> ST0001 </td>
                            <td> ABB LTD </td>
                            <td> Electronics</td>
                            <td> Mobile Panel</td>
                            <td> 5 </td>
                            <td> 23/01/2019</td>
                        </tr>
                        <tr>
                            <td> 2 </td>
                            <td> ST0002 </td>
                            <td> RITESTAR </td>
                            <td> Home Appliance </td>
                            <td> Chairs</td>
                            <td> 10 </td>
                            <td> 20/01/2019</td>
                        </tr>
                        <tr>
                            <td> 3 </td>
                            <td> ST0003 </td>
                            <td> HELUKABEL PVT LTD </td>
                            <td> Food Products </td>
                            <td> Ice Cream</td>
                            <td> 5 </td>
                            <td> 19/01/2019</td>
                        </tr>
                        <tr>
                            <td> 4 </td>
                            <td> ST0004 </td>
                            <td> FUSION</td>
                            <td> Stationary products  </td>
                            <td> Book</td>
                            <td> 50</td>
                            <td> 19/01/2019</td>
                        </tr>
                        <tr>
                            <td> 5 </td>
                            <td> ST0005 </td>
                            <td> SHIVAM PVT LTD   </td>
                            <td> Cloths</td>
                            <td> T-shirt</td>
                            <td> 20</td>
                            <td> 18/01/2019</td>
                        </tr>
                    </tbody>
                </table>
            <?php } ?>
            </div>
	        
        	
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
	$('#tblrole').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();

    $( function() {
      $("#from_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $( function() {
      $("#to_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $(document).ready(function() {
    $('.select2-hidden-accessible').select2();
    });
</script>