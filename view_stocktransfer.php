<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/edit-form.png" class="imgbasline"> View Transfer</div>
            <div class="actions">
                <a href="stocktransfer_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-angle-left"></i> Back</a>
            </div>
        </div>
        <div class="portlet-body">
            <form name="frm_data" id="frm_data" action="stocktransfer_list.php" class="form-horizontal" method="POST">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3">Store From <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="store_name" id="store_name" placeholder="Store Name" value=" Fine Line " disabled>
                        </div>
                    </div>
                     <div class="form-group">
                        <label class="control-label col-md-3">Store To <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="store_name" id="store_name" placeholder="Store Name" value=" Al Meera  " disabled>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Transfer Date <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="text" class="form-control" name="issued_date" id="issued_date" placeholder="Transfer Date" autocomplete="off" data-date-format="dd/mm/yyyy" value="28/01/2019" disabled>
                        </div>
                    </div>
                    <h3 class="form-section formheading displaytransferitem">Items</h3>
                    <div class="row">
                        <div class="col-md-12 displaytransferitem">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover" id="tblstorerecipt">
                                    <thead>
                                        <tr>
                                           <!--  <th nowrap>Item</th> -->
                                            <th class="text-center" nowrap>Part Number</th>
                                            <th nowrap>Description</th>
                                            <th nowrap class="text-center">Apple ID</th>
                                            <th nowrap class="text-center">Quantity</th>
                                            <th nowrap class="text-center">Date</th>
                                            <th nowrap>Remarks</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <!-- <td nowrap>Apple TV Base Unit</td> -->
                                            <td class="text-center" nowrap>622-00084</td>
                                            <td nowrap>Apple TV Base Unit</td>
                                            <td class="text-center">1093880</td>
                                            <td class="text-center">
                                               5
                                            </td>
                                            <td class="text-center">
                                               30/01/2019
                                            </td>
                                            <td nowrap> 
                                                QATAR APR SPARES
                                            </td>
                                        </tr>
                                        <tr>
                                            <!-- <td nowrap>Apple TV Cable Clip</td> -->
                                            <td class="text-center" nowrap>677-01423</td>
                                            <td nowrap>Apple TV Cable Clip</td>
                                            <td class="text-center">1599662</td>
                                            <td class="text-center">
                                                7
                                            </td>
                                            <td class="text-center">
                                                31/01/2019
                                            </td>
                                            <td nowrap> 
                                                QATAR APPLE SHOP 2.0 SPARES
                                            </td>
                                        </tr>
                                        <tr>
                                           <!--  <td nowrap>USBC-USBA Cable</td> -->
                                            <td class="text-center" nowrap>622-00084</td>
                                            <td nowrap>USBC-USBA Cable</td>
                                            <td class="text-center">XXXXQATAPR</td>
                                            <td class="text-center">
                                                3
                                            </td>
                                            <td class="text-center">
                                               01/02/2019
                                            </td>
                                            <td nowrap> 
                                               QATAR APR SPARES
                                            </td>
                                        </tr>
                                        <tr>
                                            <!-- <td nowrap>SR IPAD PRO WARM GPF24</td> -->
                                            <td class="text-center" nowrap>GC24806A-MEAR</td>
                                            <td nowrap>SR IPAD PRO WARM GPF24</td>
                                            <td class="text-center">XXXXQATWHITE</td>
                                            <td class="text-center">
                                                10
                                            </td>
                                            <td class="text-center">
                                               01/02/2019
                                            </td>
                                            <td nowrap> 
                                                QATAR WHITE SPARES
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                         </div>
                    </div>
                </div>
                <div class="form-actions  displaytransferitem">
                    <div class="row">
                        <div class="col-md-12 text-right">
                            <a href="stocktransfer_list.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-angle-left"></i> Back</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>
<script type="text/javascript">
    $(document).ready(function() {
    $('.select2-hidden-accessible').select2();
    });
    $(function() {
    $("#issued_date,.stock_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
});
</script>