<?php 
error_reporting(0);
$navigationURL = reset(explode("?",end(explode("/",$_SERVER["REQUEST_URI"]))));
?>
<!-- BEGIN SIDEBAR -->
<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse">
        <!-- BEGIN SIDEBAR MENU -->
        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 0px">
            <!-- BEGIN SIDEBAR TOGGLER BUTTON -->
            <li class="sidebar-toggler-wrapper hide">
                <div class="sidebar-toggler">
                    <span></span>
                </div>
            </li>
            <?php
                if (strtolower($_SESSION["user_role"]) == "admin") {
            ?>
             
            <li class="nav-item <?php echo ($navigationURL=='store_list.php' || $navigationURL=='add_store.php' ||  $navigationURL=='edit_store.php')?'active open':''; ?>">
                <a href="store_list.php" class="nav-link nav-toggle">
                    <i class="icon-warehouse"></i>
                    <span class="title">Manage Stores</span>
                    <span class="selected"></span>
                </a>
            </li>
            
            <li class="nav-item  <?php echo ($navigationURL=='supplier_list.php' || $navigationURL=='add_supplier.php' ||  $navigationURL=='edit_supplier.php')?'active open':''; ?>">
                <a href="supplier_list.php" class="nav-link nav-toggle">
                    <i class="icon-vendor"></i>
                    <span class="title">Manage Suppliers</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item <?php echo ($navigationURL=='outlet_list.php' || $navigationURL=='add_outlet.php' ||  $navigationURL=='edit_outlet.php')?'active open':''; ?>">
                <a href="outlet_list.php" class="nav-link nav-toggle">
                    <i class="icon-outlet"></i>
                    <span class="title">Manage Outlet</span>
                    <span class="selected"></span>
                </a>
            </li>
          <!--   <li class="nav-item <?php echo ($navigationURL=='category_list.php' || $navigationURL=='add_category.php' || $navigationURL=='edit_category.php' || $navigationURL=='subcategory_list.php' || $navigationURL=='add_subcategory.php' || $navigationURL=='edit_subcategory.php' || $navigationURL=='sub_subcategory_list.php' || $navigationURL=='add_sub_subcategory.php' || $navigationURL=='edit_sub_subcategory.php')?'active open':''; ?>">
                <a href="javascript:void(0);" class="nav-link nav-toggle">
                    <i class="icon-area"></i>
                    <span class="title">Manage Categories</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start <?php echo ($navigationURL=='category_list.php' || $navigationURL=='add_category.php' || $navigationURL=='edit_category.php')?'active open':''; ?>">
                        <a href="category_list.php" class="nav-link">
                            <i class="icon-category"></i>
                            <span class="title">Categories</span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo ($navigationURL=='subcategory_list.php' || $navigationURL=='add_subcategory.php' || $navigationURL=='edit_subcategory.php')?'active open':''; ?>">
                        <a href="subcategory_list.php" class="nav-link">
                            <i class="icon-subcategory"></i>
                            <span class="title">Sub Categories</span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo ($navigationURL=='sub_subcategory_list.php' || $navigationURL=='add_sub_subcategory.php' || $navigationURL=='edit_sub_subcategory.php')?'active open':''; ?>">
                        <a href="sub_subcategory_list.php" class="nav-link">
                            <i class="icon-sub-category"></i>
                            <span class="title">Sub Sub-Categories</span>
                        </a>
                    </li>
                </ul>
            </li> -->
             <li class="nav-item  <?php echo ($navigationURL=='item_list.php' || $navigationURL=='add_item.php' || $navigationURL=='edit_item.php')?'active open':''; ?>">
                <a href="item_list.php" class="nav-link nav-toggle">
                    <i class="icon-store"></i>
                    <span class="title">Manage Items</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item <?php echo ($navigationURL=='users_list.php' || $navigationURL=='add_user.php' || $navigationURL=='edit_user.php'|| $navigationURL=='role_list.php' || $navigationURL=='add_role.php' || $navigationURL=='edit_role.php')?'active open':''; ?>">
                <a href="javascript:void(0);" class="nav-link nav-toggle">
                    <i class="icon-employee"></i>
                    <span class="title">Manage Users</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <li class="nav-item start <?php echo ($navigationURL=='users_list.php' || $navigationURL=='add_user.php' || $navigationURL=='edit_user.php')?'active open':''; ?>">
                        <a href="users_list.php" class="nav-link">
                            <i class="icon-sub-user"></i>
                            <span class="title">Users</span>
                        </a>
                    </li>
                    <li class="nav-item start <?php echo ($navigationURL=='role_list.php' || $navigationURL=='add_role.php' || $navigationURL=='edit_role.php')?'active open':''; ?>">
                        <a href="role_list.php" class="nav-link">
                             <i class="icon-sub-role"></i>
                            <span class="title">Roles</span>
                        </a>
                    </li>
                </ul>
            </li>
            <?php
            }
            ?>
            <?php
             if (strtolower($_SESSION["user_role"]) == "user" || strtolower($_SESSION["user_role"]) == "deployment" || strtolower($_SESSION["user_role"]) == "maintenance" || strtolower($_SESSION["user_role"]) == "storeuser" ) {
            ?>
           <!--  <li class="nav-item  <?php echo ($navigationURL=='datainput_list.php')?'active open':''; ?>">
                <a href="datainput_list.php" class="nav-link nav-toggle">
                    <i class="icon-edit-form"></i>
                    <span class="title">Data Input Form</span>
                    <span class="selected"></span>
                </a>
            </li>-->
            <!-- <li class="nav-item  <?php echo ($navigationURL=='bulk_import_list.php' || $navigationURL=='add_bulk_import.php' || $navigationURL=='edit_bulk_import.php')?'active open':''; ?>">
                <a href="add_bulk_import.php" class="nav-link nav-toggle">
                    <i class="icon-upload"></i>
                    <span class="title">Bulk Import Of Receipts</span>
                    <span class="selected"></span>
                </a>
            </li> -->
           <!--  <li class="nav-item <?php echo ($navigationURL=='dashboard.php')?'active open':''; ?>">
                <a href="dashboard.php" class="nav-link nav-toggle">
                    <i class="dashboard-image"></i>
                    <span class="title">Dashboard</span>
                    <span class="selected"></span>
                </a>
            </li> -->
            <li class="nav-item  <?php echo ($navigationURL=='storereceipt_list.php' || $navigationURL=='add_storereceipt.php' || $navigationURL=='edit_storereceipt.php')?'active open':''; ?>">
                <a href="storereceipt_list.php" class="nav-link nav-toggle">
                    <i class="icon-upload"></i>
                    <span class="title">Stock Receipts</span>
                    <span class="selected"></span>
                </a>
            </li>
            
            <li class="nav-item  <?php echo ($navigationURL=='stocktransfer_list.php' || $navigationURL=='view_stocktransfer.php' ||$navigationURL=='add_stocktransfer.php' || $navigationURL=='edit_stocktransfer.php')?'active open':''; ?>">
                <a href="stocktransfer_list.php" class="nav-link nav-toggle">
                    <i class="icon-edit-form"></i>
                    <span class="title">Stock Transfer</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item  <?php echo ($navigationURL=='add_stockissuance.php' || $navigationURL=='edit_stockissuance.php' || $navigationURL=='stockissuance_list.php')?'active open':''; ?>">
                <a href="stockissuance_list.php" class="nav-link nav-toggle">
                    <i class="icon-disposal"></i>
                    <span class="title">Stock Issuance</span>
                    <span class="selected"></span>
                </a>
            </li> 
            <li class="nav-item  <?php echo ($navigationURL=='store_return.php' || $navigationURL=='storereturn_list.php' || $navigationURL=='edit_storereturn.php' || $navigationURL=='view_storereturn.php' )?'active open':''; ?>">
                <a href="storereturn_list.php" class="nav-link nav-toggle">
                    <i class="icon-return"></i>
                    <span class="title">Stock Return</span>
                    <span class="selected"></span>
                </a>
            </li>
            <li class="nav-item  <?php echo ($navigationURL=='stock_search.php')?'active open':''; ?>">
                <a href="stock_search.php" class="nav-link nav-toggle">
                    <i class="icon-area"></i>
                    <span class="title">Stock Search</span>
                    <span class="selected"></span>
                </a>
            </li>
            <!--  <li class="nav-item  <?php echo ($navigationURL=='store_disposal.php' || $navigationURL=='storedisposal_list.php')?'active open':''; ?>">
                <a href="storedisposal_list.php" class="nav-link nav-toggle">
                    <i class="icon-disposal"></i>
                    <span class="title">Items Disposal/Destruction</span>
                    <span class="selected"></span>
                </a>
            </li> -->
            <li class="nav-item <?php echo ( $navigationURL=='stock_report.php' || $navigationURL=='total_received.php' || $navigationURL=='total_issued.php' || $navigationURL=='total_stock.php' || $navigationURL=='total_returns.php' || $navigationURL=='total_issuance.php')?'active open':''; ?>">
                <a href="javascript:void(0);" class="nav-link nav-toggle">
                    <i class="icon-reports"></i>
                    <span class="title">Reports</span>
                    <span class="selected"></span>
                    <span class="arrow"></span>
                </a>
                <ul class="sub-menu">
                    <!-- <li class="nav-item start <?php echo ($navigationURL=='stock_report.php')?'active open':''; ?>">
                        <a href="stock_report.php" class="nav-link">
                            <span class="title">Advanced Stock Report</span>
                        </a>
                    </li> -->
                    <li class="nav-item start <?php echo ($navigationURL=='total_received.php')?'active open':''; ?>">
                        <a href="total_received.php" class="nav-link">
                            <i class="icon-received"></i>
                            <span class="title">Total Stock Receipts</span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo ($navigationURL=='total_issued.php')?'active open':''; ?>">
                        <a href="total_issued.php" class="nav-link">
                            <i class="icon-total-issued"></i>
                            <span class="title">Total Stock Transfer</span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo ($navigationURL=='total_issuance.php')?'active open':''; ?>">
                        <a href="total_issuance.php" class="nav-link">
                            <!-- <i class="icon-total-issuance"></i> -->
                            <i class="icon-stock-avl"></i>
                            <span class="title">Total Stock Issuance</span>
                        </a>
                    </li>
                    <li class="nav-item <?php echo ($navigationURL=='total_returns.php' )?'active open':''; ?>">
                        <a href="total_returns.php" class="nav-link">
                            <i class="icon-total-return"></i>
                            <span class="title">Total Stock Returns</span>
                        </a>
                    </li>
                   <!--  <li class="nav-item <?php echo ($navigationURL=='total_stock.php')?'active open':''; ?>">
                        <a href="total_stock.php" class="nav-link">
                            <i class="icon-stock-avl"></i>
                            <span class="title">Total in Stock Available</span>
                        </a>
                    </li> -->
                    <!-- <li class="nav-item <?php echo ($navigationURL=='total_destruction.php')?'active open':''; ?>">
                        <a href="total_destruction.php" class="nav-link">
                            <span class="title">Total Destructions/Disposals</span>
                        </a>
                    </li> -->
                </ul>
            </li>
            <?php
            }
            ?>
        </ul>
        <!-- END SIDEBAR MENU -->
    </div>
    <!-- END SIDEBAR -->
</div>
<!-- END SIDEBAR -->