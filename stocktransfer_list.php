<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/edit-form.png" class="imgbasline"> Stock Transfer List</div>
            <div class="actions">
                <a href="export_stock_transfer.xls" class="btn green btn-sm excelbtn"><i class="fa fa-download"></i> Export to Excel</a>
                <a href="add_stocktransfer.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Transfer</a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12 paddingleftright">
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <select class="form-control select2" name="supplier_name" id="supplier_name">
                                <option value="">Store From</option>
                                <option value="1">Al Meera</option>
                                <option value="2">Lulu Salmiya</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <select class="form-control select2" name="supplier_name" id="supplier_name">
                                <option value="">Store To</option>
                                <option value="1">Classic</option>
                                <option value="2">Fine Line</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="from_date" id="from_date" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="From Date">
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="to_date" id="to_date" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="To Date">
                        </div>
                    </div> 
                    <div class="col-md-12 text-center">
                        <div class="col-md-12 paddingleftright">
                            <button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
                            <a href="stocktransfer_list.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive" style="overflow-x: inherit;margin-top:15px;">
                <?php
                 if (strtolower($_SESSION["user_role"]) == "storeuser") {
                ?>
                <table class="table table-striped table-bordered table-hover" id="tblrole">
                    <thead>
                        <tr>
                            <th> SI.NO </th>
                            <th> Store From</th>
                            <th> Store To</th>
                            <th> Transfer Type</th>
                            <th> Date </th>
                            <th> Delivery Note </th>
                            <th> Action </th>
                        </tr>
                    </thead>
                    </tbody>
                        </tbody>
                        <tr>
                            <td> 1 </td>
                            <td> Al Meera - Warehouse </td>
                            <td> Fine Line - Warehouse  </td>
                            <td> <span class="label label-sm label-success labelradius" style="padding: 1px 13px;">Out</span></td>
                            <td> 21/01/2019 </td>
                            <td><a href="javascript:void(0);" type="button" class="btn grey-cascade btn-xs customnotebtn adddeliverynote"><i class="fa fa-file-text-o"></i> Note</a></td>
                            <td><a href="edit_stocktransfer.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a>  <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
                        </tr>
                        <tr>
                            <td> 2 </td>
                            <td> Fine Line - Warehouse </td>
                            <td> Al Meera - Warehouse </td>
                            <td> <span class="label label-sm label-info labelradius" style="padding: 1px 13px;">In</span></td>
                            <td> 22/01/2019 </td>
                            <td><a href="javascript:void(0);" type="button" class="btn grey-cascade btn-xs customnotebtn adddeliverynote"><i class="fa fa-file-text-o"></i> Note</a></td>
                            <td><a href="view_stocktransfer.php" type="button" class="btn grey-cascade btn-xs customeyebtn adddeliverynote"><i class="fa fa-eye"></i> View </a></td>
                        </tr>
                         <tr>
                            <td> 3 </td>
                            <td>Lulu Salmiya - Warehouse</td>
                            <td> Fine Line - Warehouse</td>
                            <td> <span class="label label-sm label-success labelradius" style="padding: 1px 13px;">Out</span></td>
                            <td> 23/01/2019 </td>
                            <td><a href="javascript:void(0);" type="button" class="btn grey-cascade btn-xs customnotebtn adddeliverynote"><i class="fa fa-file-text-o"></i> Note</a></td>
                            <td> <a href="edit_stocktransfer.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a>  <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
                        </tr>
                         <tr>
                            <td> 4 </td>
                            <td>Classic- Warehouse</td>
                            <td>Lulu Salmiya - Warehouse</td>
                            <td> <span class="label label-sm label-info labelradius" style="padding: 1px 13px;">In</span></td>
                            <td> 24/01/2019 </td>
                            <td><a href="javascript:void(0);" type="button" class="btn grey-cascade btn-xs customnotebtn adddeliverynote"><i class="fa fa-file-text-o"></i> Note</a></td>
                            <td> <a href="view_stocktransfer.php" type="button" class="btn grey-cascade btn-xs customeyebtn adddeliverynote"><i class="fa fa-eye"></i> View </a> </td>
                        </tr>
                    </tbody>
                </table>
                <?php
                }
                ?>
                <?php
                 if (strtolower($_SESSION["user_role"]) == "maintenance") {
                ?>
                <table class="table table-striped table-bordered table-hover" id="tblemployee">
                    <thead>
                        <tr>
                            <th> SI.NO </th>
                            <th> Transferred From <br>(Store)</th>
                            <th> Transferred To <br>(Store)</th>
                            <th> Date </th>
                            <th> Delivery Note </th>
                            <th> Action </th>
                        </tr>
                    </thead>
                    </tbody>
                        <tr>
                            <td> 1 </td>
                            <td> Fine Line Mobile Phone - Maintenance </td>
                            <td> Al Meera-deployment </td>
                            <td> 21/01/2019 </td>
                            <td><a href="javascript:void(0);" type="button" class="btn grey-cascade btn-xs customnotebtn adddeliverynote"><i class="fa fa-file-text-o"></i> Note</a></td>
                            <td><a href="edit_stocktransfer.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a>  <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
                        </tr>
                        <tr>
                            <td> 2 </td>
                            <td> Fine Line Mobile Phone - Maintenance  </td>
                            <td> Classic Mobile Phone - Deployment </td>
                            <td> 22/01/2019 </td>
                            <td><a href="javascript:void(0);" type="button" class="btn grey-cascade btn-xs customnotebtn adddeliverynote"><i class="fa fa-file-text-o"></i> Note</a></td>
                            <td><a href="view_stocktransfer.php" type="button" class="btn grey-cascade btn-xs customeyebtn adddeliverynote"><i class="fa fa-eye"></i> View</a></td>
                        </tr>
                         <tr>
                            <td> 3 </td>
                            <td> Fine Line Mobile Phone - Maintenance  </td>
                            <td> Ibrar Shop - Maintenance  </td>
                            <td> 23/01/2019 </td>
                            <td><a href="javascript:void(0);" type="button" class="btn grey-cascade btn-xs customnotebtn adddeliverynote"><i class="fa fa-file-text-o"></i> Note</a></td>
                            <td><a href="view_stocktransfer.php" type="button" class="btn grey-cascade btn-xs customeyebtn adddeliverynote"><i class="fa fa-eye"></i> View</a></td>
                        </tr>
                         <tr>
                            <td> 4 </td>
                            <td> Fine Line Mobile Phone- Maintenance </td>
                            <td> Carrefour- Deployment </td>
                            <td> 24/01/2019 </td>
                            <td><a href="javascript:void(0);" type="button" class="btn grey-cascade btn-xs customnotebtn adddeliverynote"><i class="fa fa-file-text-o"></i> Note</a></td>
                            <td>   <a href="edit_stocktransfer.php" type="button" class="btn grey-cascade btn-xs custominvitebtn"><i class="fa fa-edit"></i> Edit</a>  <a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i> Delete</a> </td>
                        </tr>
                    </tbody>
                </table>
                <?php
                }
                ?>
            </div>
        </div>
    </div>
</div>
<div class="modal fade in" id="deliverynote" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title"><b>Add Delivery Note</b></h4>
            </div>
            <div class="modal-body"> 
                <div class="row">
                    <div class="col-md-12" style="margin-top: 10px;margin-bottom: 15px;">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label class="control-label col-md-12">Delivery Note</label>
                                <div class="col-md-12">
                                    <textarea  type="text" class="form-control" name="note" id="note" cols="10" rows="10">Delivered</textarea>
                                </div>
                            </div>
                        </div>
                    </div>
                   <!--  <div class="col-md-12">
                        <table class="table table-striped table-bordered table-hover" id="tblstorerecipt">
                            <thead>
                                <tr>
                                    <th class="text-center" nowrap  style="width:40px;">S.NO</th>
                                    <th class="text-center">Delivery Note</th>
                                    <th class="text-center" style="width:40px;">Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-center" nowrap>1</td>
                                    <td class="text-center">Not yet Shipped</td>
                                    <td class="text-center"><a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i></a></td>
                                </tr>
                                <tr>
                                    <td class="text-center" nowrap>2</td>
                                    <td class="text-center">Shipping soon</td>
                                    <td class="text-center"><a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i></a></td>
                                </tr>
                                <tr>
                                    <td class="text-center" nowrap>3</td>
                                    <td class="text-center">Dispatched</td>
                                    <td class="text-center"><a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i></a></td>
                                </tr>
                                <tr>
                                    <td class="text-center" nowrap>4</td>
                                    <td class="text-center">Out for Delivery</td>
                                    <td class="text-center"><a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i></a></td>
                                </tr>
                                <tr>
                                    <td class="text-center" nowrap>5</td>
                                    <td class="text-center">Delivered</td>
                                    <td class="text-center"><a href="javascript:void(0);" type="button" class="btn btn-danger btn-xs customactionredbtn"><i class="fa fa-trash"></i></a></td>
                                </tr>
                            </tbody>
                        </table>
                    </div> -->
                </div>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn green customsavebtn" data-dismiss="modal"> <i class="fa fa-check"></i>Add Note</button>
                <button type="button" class="btn red customrestbtn"  data-dismiss="modal"> <i class="fa fa-close"></i> Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>
<script>
    $(document).ready(function() {
    $('#tblrole,#tblemployee').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();
    $(document).ready(function() {
        $('.select2-hidden-accessible').select2();
    });
    $( function() {
      $("#from_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $( function() {
      $("#to_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $(document).ready(function () {
        $(".adddeliverynote").click(function(){
             $('#deliverynote').modal('show');
        });
    });
</script>