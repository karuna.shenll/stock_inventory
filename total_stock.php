<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/reports.png" class="imgbasline"> Total in Stock Available</div>
            <div class="actions">
               <a href="javascript:void(0);" class="btn green btn-sm customviewbtn bulkpopup"><i class="fa fa-print"></i> Print </a>
                <a href="total_stock_available.xls" class="btn green btn-sm excelbtn"><i class="fa fa-download"></i> Export to Excel</a>
            </div>
        </div>
        <div class="portlet-body">
            <div class="row">
                <div class="col-md-12 paddingleftright">
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                            <select class="form-control select2" name="supplier_id" id="supplier_id">
                                <option value="">Select Supplier</option>
                                <option value="Supplier">Arvato</option>
                                <option value="Supplier">ARIAN</option>
                                <option value="Supplier">CCS DIGITAL</option>
                                <option value="Supplier">FUSION</option>
                                <option value="Supplier">SHIVAM</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="serial_no" id="serial_no" autocomplete="off"  placeholder="Part Number">
                        </div>
                    </div>
                      <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="serial_no" id="serial_no" autocomplete="off"  placeholder="Part Description">
                        </div>
                    </div>
                      <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="serial_no" id="serial_no" autocomplete="off"  placeholder="Apple ID">
                        </div>
                    </div>
                   
                   <!--  <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="from_date" id="from_date" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="From Date">
                        </div>
                    </div>
                    <div class="col-md-3 paddingbottom">
                        <div class="col-md-12 paddingleftright">
                           <input type="text" class="form-control" name="to_date" id="to_date" autocomplete="off" data-date-format="dd/mm/yyyy" placeholder="To Date">
                        </div>
                    </div> -->
                    <div class="col-md-12 text-center ">
                        <div class="col-md-12 paddingleftright">
                            <button type="button" class="btn btn-warning customsearchtbtn"> <i class="fa fa-search"></i> Search</button>
                            <a href="total_stock.php" class="btn red customrestbtn"> <i class="fa fa-refresh"></i> Reset</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
                <table class="table table-striped table-bordered table-hover" id="tblrole">
                    <thead>
                        <tr>
                            <th nowrap> SI.NO </th>
                            <th nowrap> Supplier Code</th>
                            <th nowrap> Supplier Name</th>
                            <th nowrap> Part Number </th>
                            <th nowrap> Part Description </th>
                            <th nowrap> Apple ID </th>
                            <th nowrap> Avaliable Stock Qty </th>
                            <!-- <th nowrap> Destructions / Disposals Date </th> -->
                        </tr>
                    </thead>
                    </tbody>
                        <tr>
                            <td> 1 </td>
                            <td> 1001 </td>
                            <td> Arvato </td>
                            <td> 82989829 </td>
                            <td> Cash Wrap Q318</td>
                            <td> 546881 </td>
                            <td> 100 </td>
                            <!-- <td> 23/01/2019</td> -->
                        </tr>
                        <tr>
                            <td> 2 </td>
                            <td> 1002 </td>
                            <td> ARIAN </td>
                            <td> 82989830 </td>
                            <td> WARM GPF24</td>
                            <td> 651017 </td>
                            <td> 200 </td>
                            <!-- <td> 20/01/2019</td> -->
                        </tr>
                        <tr>
                            <td> 3 </td>
                            <td> 1003 </td>
                            <td> CCS DIGITAL </td>
                            <td> 82989831 </td>
                            <td> Cables rj45</td>
                            <td> 1465542 </td>
                            <td> 300 </td>
                           <!--  <td> 19/01/2019</td> -->
                        </tr>
                        <tr>
                            <td> 4 </td>
                            <td> 1004 </td>
                            <td> FUSION</td>
                            <td> 82989832 </td>
                            <td> Wall Panel Q318</td>
                            <td>1093880</td>
                            <td> 400</td>
                           <!--  <td> 19/01/2019</td> -->
                        </tr>
                    </tbody>
                </table>
            </div>
              <div id="container" style="min-width: 310px; height: 400px; margin: 0 auto"></div>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
    $('#tblrole').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    } );    
    } );
    $("#search_result_length").hide();
    $(document).ready(function() {
        $('.select2-hidden-accessible').select2();
    });
    $( function() {
      $("#from_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    $( function() {
      $("#to_date").datepicker({ todayHighlight: true,dateFormat: 'dd/mm/yy',autoclose: true });
    });
    Highcharts.chart('container', {
    chart: {
        type: 'column'
    },
    title: {
        text: 'Supplier Wise Stock Available.'
    },
    subtitle: {
        // text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
    },
    xAxis: {
        type: 'category',
        title: {
            text: 'Supplier Name'
        }
    },
    yAxis: {
        title: {
            text: 'Supplier wise Stock Available'
        }

    },
    legend: {
        enabled: false
    },
    plotOptions: {
        series: {
            borderWidth: 0,
            dataLabels: {
                enabled: true,
                format: '{point.y:f}'
            }
        }
    },

    tooltip: {
        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> of Quantity<br/>'
    },

    "series": [
        {
            "name": "Store",
            "colorByPoint": true,
            "data": [
                {
                    "name": "Arvato",
                    "y": 100,
                    "drilldown": "Arvato"
                },
                {
                    "name": "ARIAN",
                    "y": 200,
                    "drilldown": "ARIAN"
                },
                {
                    "name": "CCS DIGITAL",
                    "y": 300,
                    "drilldown": "CCS DIGITAL"
                },
                {
                    "name": "FUSION",
                    "y": 400,
                    "drilldown": "FUSION"
                }
            ]
        }
    ],
    "drilldown": {
        "series": [
            {
                "name": "Arvato",
                "id": "Arvato",
                "data": [
                    [
                        "Cash Wrap Q318",
                        12
                    ],
                    [
                        "WARM GPF24",
                        10
                    ],
                    [
                        "Cables rj45",
                        25
                    ],
                    [
                        "Wall Panel Q318",
                        10
                    ],
                ]
            },
            {
                "name": "ARIAN",
                "id": "ARIAN",
                "data": [
                    [
                        "Cash Wrap Q318",
                        10
                    ],
                    [
                        "WARM GPF24",
                        20
                    ],
                    [
                        "Cables rj45",
                        12
                    ],
                    [
                        "Wall Panel Q318",
                        15
                    ]
                ]
            },
            {
                "name": "CCS DIGITAL",
                "id": "CCS DIGITAL",
                "data": [
                    [
                        "Cash Wrap Q318",
                        70
                    ],
                    [
                        "WARM GPF24",
                        30
                    ],
                    [
                        "Cables rj45",
                        20
                    ],
                    [
                        "Wall Panel Q318",
                        45
                    ]
                ]
            },
            {
                "name": "FUSION",
                "id": "FUSION",
                "data": [
                    [
                        "Cash Wrap Q318",
                        18
                    ],
                    [
                        "WARM GPF24",
                        22
                    ],
                    [
                        "Cables rj45",
                        14
                    ],
                    [
                        "Wall Panel Q318",
                        50
                    ]
                ]
            }
        ]
    }
});
</script>