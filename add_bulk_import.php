<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<!-- BEGIN CONTENT BODY -->
<div class="page-content">
    <div class="portlet box blue boardergrey">
        <div class="portlet-title">
            <div class="caption">
                <img src="assets/layouts/layout/img/de-active/upload.png" class="imgbasline"> Bulk Import Of Receipts</div>
            <div class="actions">
                <!-- <a href="add_sub_subcategory.php" class="btn green btn-sm customaddbtn"><i class="fa fa-plus"></i> Add Sub Sub-Category</a> -->
            </div>
        </div>
        <div class="portlet-body">
            <form name="frm_user" id="frm_user" action="add_bulk_import.php" class="form-horizontal" method="POST">
                <div class="form-body">
                    <div class="form-group">
                        <label class="control-label col-md-3"> Store Name <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <select class="form-control" name="supplier_code" id="supplier_code">
                                <option value="">Select Store</option>
                                <option value="1">Al Meera</option>
                                <option value="2">Lulu Salmiya</option>
                                <option value="3">Defence Co Op</option>
                                <option value="4">Carrefour</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Bulk Upload <span class="required" aria-required="true"> * </span>
                        </label>
                        <div class="col-md-4">
                            <input type="file" class="form-control" name="bulk_upload" id="bulk_upload">
                            <span class="help-block">(Upload File Format: .xls, .xlxs)</span>
                            <p class="downloadexcel"><a href="download.xls"><img src="../assets/pages/img/download.png"> Click Here To Download Bulk Upload Excel Format</a></p>
                        </div>
                        <div class="col-md-offset-3 col-md-9">
                            <input type="button" class="btn green customsavebtn" value="Add Document" id="excel_view_click">
                            <a href="add_bulk_import.php" class="btn red customrestbtn" id="resetEmpty"> <i class="fa fa-refresh"></i> Cancel</a>
                        </div>
                    </div>
                </div>
            <div id="excel_view" style="display: none">
                <div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
                <table class="table table-striped table-bordered table-hover" id="tblvendor">
                    <thead>
                        <tr>
                            <!-- <th nowrap> SI.NO </th> -->
                           <!--  <th nowrap> Supplier Code </th> -->
                            <th nowrap> Serial No </th>
                            <th nowrap> Item Name </th>
                            <th nowrap> Quantity </th>
                            <th nowrap> Description </th>
                            <th nowrap> Unit Price </th>
                            <th nowrap> Total </th>
                               
                        </tr>
                    </thead>
                    </tbody>
                        <tr>
                           <!--  <td nowrap> 1 </td> -->
                            <!-- <td> 10609 </td>  -->
                            <td nowrap> 18367848 </td>
                            <td nowrap> Sony Mobile</td>
                            <td nowrap> 20 </td>
                            <td nowrap> good </td>
                            <td nowrap> 1 </td>
                            <td nowrap> 20 </td>
                        </tr>
                        <tr>
                            <!-- <td nowrap> 2 </td> -->
                            <!-- <td nowrap> 13423 </td> -->
                            <td nowrap> 18367849 </td>
                            <td nowrap> Apple Mobile</td>
                            <td nowrap> 30 </td>
                            <td nowrap> good </td>
                            <td nowrap> 2 </td>
                            <td nowrap> 60 </td>
                        </tr>
                        <tr>
                            <!-- <td nowrap> 3 </td> -->
                           <!--  <td> 28200 </td> -->
                            <td nowrap> 18367850 </td>
                            <td nowrap> Nokia Mobile</td>
                            <td nowrap> 5 </td>
                            <td nowrap> good </td>
                            <td nowrap> 50 </td>
                            <td nowrap> 250 </td>
                        </tr>
                        <tr>
                            <!-- <td nowrap> 4 </td> -->
                            <!-- <td nowrap> 13219 </td> -->
                            <td nowrap> 18367851 </td>
                            <td nowrap> MI Mobile</td>
                            <td nowrap> 1 </td>
                            <td nowrap> good </td>
                            <td nowrap> 1000 </td>
                            <td nowrap> 1000 </td>
                        </tr>
                    </tbody>
                </table>
            </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-12" align="right">
                            <button type="submit" class="btn green customsavebtn">
                                <i class="fa fa-check"></i> Upload
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            </form>
        </div>
    </div>
</div>
<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>

<script>
    $(document).ready(function() {
	$('#tblrole').DataTable( {
        "bPaginate": true,
         "bLengthChange": false,
        "bFilter": false,
        "bInfo": false,
        "iDisplayLength":5 ,
        "ordering": false
    });    
    });
    $("#search_result_length").hide();

    $("#excel_view_click").click(function () {
    $("#excel_view").css("display", "block");
});
     $(document).ready(function() {
    $('.select2-hidden-accessible').select2();
    });
</script>