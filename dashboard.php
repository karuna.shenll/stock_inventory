<?php 
session_start();
include("session_check.php"); 
include("header.php"); 
?>
<script src="https://code.highcharts.com/highcharts.js"></script>
<script src="https://code.highcharts.com/modules/data.js"></script>
<script src="https://code.highcharts.com/modules/drilldown.js"></script>
<!-- BEGIN CONTENT BODY -->
<div class="page-content"  id="dashboard" style="background-color:#FFFFFF">
	<div class="row widget-row">
        <div class="col-md-12 paddingleftright">
            <div class="col-md-8 paddingbottom"></div>
            <div class="col-md-2 paddingbottom">
                <div class="col-md-12 paddingleftright">
                    <select class="form-control select2" name="month" id="month" >
                        <option value="">Month</option>
                        <option selected value='1'>January</option>
                        <option value='2'>February</option>
                        <option value='3'>March</option>
                        <option value='4'>April</option>
                        <option value='5'>May</option>
                        <option value='6'>June</option>
                        <option value='7'>July</option>
                        <option value='8'>August</option>
                        <option value='9'>September</option>
                        <option value='10'>October</option>
                        <option value='11'>November</option>
                        <option value='12'>December</option>
                    </select>
                </div>
            </div>
            <div class="col-md-2 paddingbottom">
                <div class="col-md-12 paddingleftright">
                    <select class="form-control select2" name="year" id="year" >
                        <option value="">Year</option>
                            <option>2017</option>
                            <option>2018</option>
                            <option selected>2019</option>
                    </select>
                </div>
            </div>
        </div>
            
        <!-- <div class="col-md-3">
            <div class="widget-thumb dashboard-stat red text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Total Request Received</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white  fa fa-cart-plus redicon dashboardborder"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="5585"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-thumb dashboard-stat bg-green-jungle text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Total Issued</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white fa icon-handbag  font-green-jungle dashboardborder"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="5085"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-thumb dashboard-stat yellow-casablanca text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Total Returns</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white fa fa-exchange dashboardborder font-yellow-casablanca"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="598"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="widget-thumb dashboard-stat blue text-uppercase margin-bottom-20 bordered">
                <h4 class="widget-thumb-heading">Total Suppliers</h4>
                <div class="widget-thumb-wrap">
                    <i class="widget-thumb-icon bg-white font-blue fa fa-building dashboardborder"></i>
                    <div class="widget-thumb-body">
                        <span class="widget-thumb-body-stat" data-counter="counterup" data-value="35"></span>
                    </div>
                </div>
            </div>
        </div> -->
        <div class="col-md-12">
            <div class="table-responsive" style="overflow-x: inherit;margin-top:0px;">
                    <table class="table table-striped table-bordered table-hover" id="tblrole">
                        <thead>
                            <tr>
                                <th nowrap> SI.NO </th>
                                <th nowrap> Store Name </th>
                                <th nowrap> Part Number </th>
                                <th nowrap> Part Description </th>
                                <th nowrap> Apple ID </th>
                                <th nowrap> Transferred Qty </th>
                                <th nowrap> Transferred Date </th>
                            </tr>
                        </thead>
                        </tbody>
                            <tr>
                                <td> 1 </td> 
                                <td> Al Meera </td>
                                <td> 818-02365 </td>
                                <td> Base Unit </td>
                                <td> 546881 </td>
                                <td> 5</td>
                                <td> 23/01/2019</td>
                            </tr>
                            <tr>
                                <td> 2 </td>
                                <td> Lulu Salmiya </td>
                                <td> 153-0752-00 </td>
                                <td> Standing Acylic</td>
                                <td> 651017 </td>
                                <td> 4</td>
                                <td> 20/01/2019</td>
                            </tr>
                            <tr>
                                <td> 3 </td>  
                                <td> Classic Mobile Phone </td>                        
                                <td> SK-T12MAA-W-395 </td>
                                <td> Cable Clip </td>
                                <td> 1465542 </td>
                                <td> 3</td>
                                <td> 19/01/2019</td>
                            </tr>
                            <tr>
                                <td> 4 </td>  
                                <td> Ibrar Shop  </td>                        
                                <td> 3C523Z/A </td>
                                <td> Cables rj45</td>
                                <td> 1093880 </td>
                                <td> 7</td>
                                <td> 19/01/2019</td>
                            </tr>
                            <tr>
                                <td> 5 </td>  
                                <td> Classic Mobile Phone </td>                          
                                <td> HA54-46-45 </td>
                                <td> Angled acrylic</td>
                                <td> 1599662 </td>
                                <td> 10</td>
                                <td> 18/01/2019</td>
                            </tr>
                            <tr>
                                <td> 6 </td>     
                                <td> Al Meera </td>                       
                                <td> CH0010 </td>
                                <td> Tophat cable</td>
                                <td> 1599661 </td>
                                <td> 7</td>
                                <td> 18/01/2019</td>
                            </tr>
                        </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- END CONTENT BODY -->
<?php 
include("footer.php"); 
?>
<script type="text/javascript">
     $(document).ready(function() {
        $('.select2-hidden-accessible').select2();
    });
    $(document).ready(function(){
        var category = $("#category_name").val();
        var chart_data = '';
        if(category=="1"){
            chart_data=[
                {
                    "name": "Base Unit",
                    "y": 62
                },
                {
                    "name": "Standing Acylic",
                    "y": 10
                },
                {
                    "name": "Cable Clip",
                    "y":40
                },
                {
                    "name": "Cables rj45",
                    "y": 18
                },
                {
                    "name": "angled acrylic",
                    "y": 14
                },
                 {
                    "name": "tophat cable",
                    "y": 8
                }
            ]
        }
        drawChart(chart_data);
    });
    $(document).on("change","#category_name",function(){
        var category = $(this).val();
        if(category=="1"){
            chart_data=[
                {
                    "name": "Base Unit",
                    "y": 62
                },
                {
                    "name": "Standing Acylic",
                    "y": 10
                },
                {
                    "name": "Cable Clip",
                    "y":40
                },
                {
                    "name": "Cables rj45",
                    "y": 18
                },
                {
                    "name": "angled acrylic",
                    "y": 14
                },
                 {
                    "name": "tophat cable",
                    "y": 8
                }
            ]
        }
        if(category=="2"){
             chart_data=[
                {
                    "name": "PINK GPF21",
                    "y": 20
                },
                {
                    "name": "WARM GPF24",
                    "y": 40
                },
                {
                    "name": "Wall Panel Q318",
                    "y":30
                },
                {
                    "name": "Pro Pink GPF24",
                    "y": 17
                },
                {
                    "name": "Angled acrylic",
                    "y": 18
                },
                 {
                    "name": "Tophat cable",
                    "y": 8
                },
                {
                    "name": "Cable Clip",
                    "y": 34
                }
            ]
        }
        if(category=="3"){
             chart_data=[
                {
                    "name": "Sport Band",
                    "y": 10
                },
                {
                    "name": "Nike Sport Band",
                    "y": 5
                },
                {
                    "name": "AirPods",
                    "y":40
                },
                {
                    "name": "Cables rj45",
                    "y": 18
                },
                {
                    "name": "angled acrylic",
                    "y": 34
                },
                 {
                    "name": "tophat cable",
                    "y": 67
                },
                {
                    "name": "Cable Clip",
                    "y": 37
                }
            ]
        }
        if(category=="4"){
             chart_data=[
                {
                    "name": "Base Unit",
                    "y": 10
                },
                {
                    "name": "Standing Acylic",
                    "y": 70
                },
                {
                    "name": "USBA Cable",
                    "y":32
                },
                {
                    "name": "Cables rj45",
                    "y": 48
                },
                {
                    "name": "angled acrylic",
                    "y": 52
                },
                 {
                    "name": "tophat cable",
                    "y": 44
                },
                {
                    "name": "Cable Clip",
                    "y": 19
                }
            ]
        }
        if(category=="5"){
            chart_data=[
                {
                    "name": "FABRIC PANEL GPF 25",
                    "y": 50
                },
                {
                    "name": "FABRIC PANEL GPF 1",
                    "y": 30
                },
                {
                    "name": "FABRIC PANEL GPF 16 ",
                    "y":40
                },
                {
                    "name": "FABRIC PANEL GPF 5 ",
                    "y": 20
                },
                {
                    "name": "PINK GPF21",
                    "y": 76
                },
                 {
                    "name": "Base Kit",
                    "y": 100
                },
                {
                    "name": "WARM GPF24",
                    "y": 120
                }
            ]
        }
        drawChart(chart_data);
    });
// Create the chart
function drawChart(chart_data) {
    Highcharts.chart('container', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Categories wise stock issued in January, 2019'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            type: 'category',
            title: {
                text: 'Item Name'
            }
        },
        yAxis: {
            title: {
                text: 'Categories wise issued Qty'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:f}'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:f}</b> of total<br/>'
        },
        
        "series": [

            {
                "name": "Items",
                "colorByPoint": true,
                "data":chart_data
            }
        ]
    });
}
</script>